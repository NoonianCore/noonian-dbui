function (db, invoker, auth, Q, _, logger) {
    logger = logger.get('DbuiMenuBuilder');
    
    var exports = {};
    var ENGLISH_ID='-9vPfv2lEeSFtiimx_V4dw';


    //Helper function to tidy up any useless menu items
    const cleanupMenu = function(menuItemList) {
        for(var i=0; i < menuItemList.length; i++) {
            var m = menuItemList[i];
            if(!m.action && !m.submenu && !m.definition_getter) {
                menuItemList.splice(i, 1);
                i--;   
            }
        }
        return menuItemList;
    };
    
    
    /**
     * DbuiMenuBuilder.buildMenu
     * takes a key, queryies for the menu, and dereferences any submenus
     * @return array of objects
     */ 
    var buildMenu = 
    exports.buildMenu = function(key, user, parentMenu) {
        var lang = user && user.language ? user.language._id : ENGLISH_ID;
        
        
        return Q.all([
            db.Menu.findOne({key:key, rolespec:{$satisfiedBy:user.roles}}),
            db.LabelGroup.findOne({key, 'language._id':lang})
        ]).spread(
            function(menuObj, labelGroup) {
                
                if(!menuObj) {
                    logger.verbose(`Invalid menu key '${key}' for user '${user.name}'`);
                    return;
                }
                
                const labels = labelGroup && labelGroup.value || {};
                const ret = [];
                if(parentMenu) {
                    parentMenu.submenu = ret;
                }
                
                // phase 1 resolve definition
                var defPromise;
                if(!menuObj.type || menuObj.type === 'static') {
                    let menuDef = menuObj.definition || [];
                    if(!Array.isArray(menuDef)) {
                        menuDef = [menuDef];
                    }
                    defPromise = Q(menuDef);
                }
                else if(menuObj.type === 'clientside') {
                    defPromise = Q({
                        definition_getter:''+menuObj.definition_getter
                    });
                }
                if(menuObj.type === 'serverside') {
                    defPromise = invoker.invokeAndReturnPromise(menuObj.definition_getter, {user, parentMenu}, menuObj);
                }
                
                
                
                return defPromise.then(menuDef=>{
                    const promiseList = [];
                
                    if(parentMenu && labels._submenu_label) {
                        parentMenu.label = labels._submenu_label;
                    }
                    
                    if(Array.isArray(menuDef)) {
                        for(var j=0; j<menuDef.length; j++) {
                            var menuItem = menuDef[j];
                            ret.push(menuItem);
                            
                            //Replace label with one referenced in LabelGroup
                            if(labels && labels[menuItem.label]) {
                                menuItem.label = labels[menuItem.label];
                            }
                            
                            //Is it a ref? 
                            if(menuItem.ref) {
                                promiseList.push(
                                    buildMenu(menuItem.ref, user, menuItem)
                                );
                            }
                        }
                    }
                    else {
                        //Not a list of menu items; clientside getter
                        if(parentMenu) {
                            parentMenu.submenu = menuDef;
                        }
                        return menuDef;
                    }
                    
                    if(promiseList.length > 0) {
                        return Q.all(promiseList).then(function() { return cleanupMenu(ret)});
                    }
                    else {
                        return cleanupMenu(ret);
                    }
                },
                err=>{
                    console.error('bad menu definition: %s', key);
                    console.error(err);
                });
            }
        );
    
    };
    
    return exports;
}