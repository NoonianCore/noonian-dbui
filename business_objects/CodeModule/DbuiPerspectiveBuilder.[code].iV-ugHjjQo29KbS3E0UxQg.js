function (db, auth, Config, Q, _, logger) {
    const config = Config;
    
    var exports = {};
    
    logger = logger.get('DbuiPerspectiveBuilder');
    
    //Manual 'hoisting' of functions
    var autogenPerspective;
    var getPerspective;
    var getDisplayOptions;
    var resolveUsePerspective;
    
    
    
    //Map perspective type to a function that generates a default perspective of that type:
    const perspectiveGenerators = {  
        
        list:function(targetTypeDescMap, perspectiveConfigItem) {
            perspectiveConfigItem.list.fields = [];
            for(var f in targetTypeDescMap) {
                if(f.indexOf('_') === 0) continue;
                perspectiveConfigItem.list.fields.push(f);
            }
        },
        
        picker_list:function(targetTypeDescMap, perspectiveConfigItem) {
            if(perspectiveConfigItem.list) {
                perspectiveConfigItem.picker_list = perspectiveConfigItem.list;
            }
            else {
                perspectiveConfigItem.picker_list.fields = [];
                for(var f in targetTypeDescMap) {
                    if(f.indexOf('_') === 0) continue;
                    perspectiveConfigItem.picker_list.fields.push(f);
                }
            }
        },
        
        view:function(targetTypeDescMap, perspectiveConfigItem) {
            perspectiveConfigItem.view.layout = [];
              for(var f in targetTypeDescMap) {
                  if(f.indexOf('_') === 0) continue;
                  perspectiveConfigItem.view.layout.push(f);
              }
        },
        
        'dialog-view':function(targetTypeDescMap, perspectiveConfigItem) {
            if(perspectiveConfigItem.view) {
                perspectiveConfigItem['dialog-view'] = perspectiveConfigItem.view;
            }
            else {
              perspectiveConfigItem['dialog-view'].layout = [];
            
              for(var f in targetTypeDescMap) {
                  if(f.indexOf('_') === 0) continue;
                  perspectiveConfigItem['dialog-view'].layout.push(f);
              }
            }
        },
        
        edit: function(targetTypeDescMap, perspectiveConfigItem) {
            perspectiveConfigItem.edit.layout = [];
              for(var f in targetTypeDescMap) {
                  if(f.indexOf('_') === 0) continue;
                  perspectiveConfigItem.edit.layout.push(f);
              }
        }
    };
    
    /**
     * @private
     */
    const getTypeDescMap = function(boClass) {
        
        if(db[boClass]) {
            return db[boClass]._bo_meta_data.type_desc_map;
        }
        else if(boClass.indexOf('#') > 0) {
            var hashPos = boClass.indexOf('#');
            var baseClass = boClass.substring(0, hashPos);
            var subField = boClass.substring(hashPos+1, boClass.length).replace(/#/g, '.');
            
            var fieldTd = db[baseClass]._bo_meta_data.getTypeDescriptor(subField);
            if(fieldTd) {
                if(fieldTd instanceof Array) {
                    fieldTd = fieldTd[0];
                }
                return fieldTd.type_desc_map;
            }
        }
        else {
            throw new Error('Invalid class name '+boClass);
        }
        
    };
    
    /*
        arrayMerger is passed to _.mergeWith() so array merge behavior as follows:
        Accept the 2nd param as the merged value, ignoring value in 1st,
        UNLESS "_append" key is present, in which case two arrays are concatenated 
        
    */
    var arrayMerger = function(objectVal, sourceVal) {
        let obj = _.get(objectVal, '_append');
        let src = _.get(sourceVal, '_append');
        let append = !!(obj || src);
        
        obj = obj || objectVal;
        src = src || sourceVal;  
        
        if(_.isArray(obj) || _.isArray(src)) {
            if(!append) {
              return src;
            }
            else {
              return _.concat(obj||[], src||[]);
            }
        }
        else {
            return undefined; //default merger
        }
    };
    
    /*
      array merger should've handled array appends in _.mergeWith
      but some may have been skipped if the key isn't present in both objects;
      cleanupAppends takes care of those
    */
    const cleanupAppends = function(obj) {
        _.forEach(obj, (v,k)=>{
            if(v && v._append) {
                obj[k] = v._append;
            }
            else if(_.isObject(v)) {
                cleanupAppends(v);
            }
        })
    };
    
    /**
     * generate and save a default perspective
     */
    autogenPerspective = function(boClass, perspectiveType) {
        logger.info(`Auto-generating default ${perspectiveType} perspective for ${boClass}`);
        
        if(perspectiveGenerators[perspectiveType]) {
            var key = 'sys.dbui.perspective.default.'+boClass;
            
            return Config.getParameter(key, {}).then(perspectiveConfigItem=>{
                perspectiveConfigItem[perspectiveType] = {};
                
                perspectiveGenerators[perspectiveType](getTypeDescMap(boClass), perspectiveConfigItem);
                
                return Config.saveParameter(key, perspectiveConfigItem)
                    .then(()=>perspectiveConfigItem);
            });
        }
        else {
            return Q.reject('Unable to auto-generate perspective type '+perspectiveType);
        }
    }
    
    /**
     * DbuiPerspectiveBuilder.getDisplayOptions
     * @private
     *  Merges sys.dbui.displayoptions w/ sys.dbui.displayoptions.SpeecificClass
     *  @return an object: {field_name:{customizations}}
     **/
    getDisplayOptions = function(boClass, userId, perspectiveName, perspectiveType) {
      var rootKey = 'sys.dbui.displayoptions';
      var classKey = rootKey+'.'+boClass;
    
    
      var promiseArray = [
          Config.getCustomizedParameter(rootKey, userId),
          Config.getCustomizedParameter(classKey, userId)
      ];
        
      //Also, incorporate composite field 'sub-perspectives' 
      var typeDescMap = getTypeDescMap(boClass);//db[boClass]._bo_meta_data.type_descriptor;
    //   var composites = [];
      
    //   _.forEach(typeDescMap, function(td, fieldName) {
    //       if(td.type === 'composite') {
    //           composites.push(fieldName);
    //           promiseArray.push(getPerspective(boClass+'#'+fieldName, perspectiveName, perspectiveType, userId));
    //       }
    //       //TODO check for named composites???
    //   });
    
      return Q.all(promiseArray).then(function(resultArr) {
        var rootObj = resultArr[0] || {};
        var classObj = resultArr[1] || {};
        var result = {};
        
        //the base sys.dbui.displayoptions contains params for specific field types.
        // the class-specific one contains params based on field name...
    
        //Iteratate through the type descriptor's to map params to appropriate field names:
        _.forEach(typeDescMap, function(td, fieldName) {
          if(classObj[fieldName]) {
            result[fieldName] = rootObj[td.type] ? _.clone(rootObj[td.type]) : {};
            _.mergeWith(result[fieldName], classObj[fieldName], arrayMerger);
          }
          else if(rootObj[td.type]) {
            result[fieldName] = _.clone(rootObj[td.type]);
          }
        });
        
        //Merge any composite 'sub-perspectives' into result
        // var arrIndex = 2; //remainder of resultArr are result of getPerspective() calls
        // _.forEach(composites, function(fieldName) {
        //     if(!result[fieldName]) {
        //         result[fieldName] = {};
        //     }
        //     if(!result[fieldName].perspective) {
        //         result[fieldName].perspective = {};
        //     }
        //     result[fieldName].perspective[perspectiveType] = resultArr[arrIndex];
        //     arrIndex++;
        // });
    
        return result;
      });
    }
    
    
    resolveUsePerspective = 
    exports.resolveUsePerspective = function(perspectiveObj, boClass, perspectiveName, perspectiveType, userId) {
        
        const derefPerspective = perspectiveObj && perspectiveObj[perspectiveType]  && perspectiveObj[perspectiveType].usePerspective;
        
        if(!derefPerspective) {
            return Q(perspectiveObj);
        }
        
        delete perspectiveObj[perspectiveType].usePerspective;
        
        return getPerspective(boClass, derefPerspective, perspectiveType, userId).then(usePersp=>{
            
            var result = {};
            result[perspectiveType] = usePersp;
            _.assign(result[perspectiveType], perspectiveObj[perspectiveType]); //content in the referencING perspective should override the referenced one
            
            return result;
        });
        
    };
    
    
    
    /**
     * DbuiPerspectiveBuilder.getPerspective
     */
    getPerspective = 
    exports.getPerspective = function(boClass, perspectiveName, perspectiveType, userId) {
        
        var rootKey = 'sys.dbui.perspective';         //any perspective, any BO
        var baseKey = rootKey+'.'+perspectiveName;    //specific perspective, any BO
        var classKey = baseKey+'.'+boClass;           //specific perspective, specific BO
        var wildcardKey = rootKey+'.*.'+boClass;      //any perspective, specific BO
        
        var defaultKey = rootKey+'.default.'+boClass; //default class-specific perspective
        
        return Q.all([
            Config.getCustomizedParameter(rootKey, userId),
            Config.getCustomizedParameter(baseKey, userId),
            Config.getCustomizedParameter(wildcardKey, userId),
            Config.getCustomizedParameter(classKey, userId),
            Config.getCustomizedParameter(defaultKey, userId),
            getDisplayOptions(boClass, userId, perspectiveName, perspectiveType)
        ])
        .spread((root,base,wild,clazz,defaultClazz,displayOptions)=>{
            const configArray = [
                root,
                base,
                wild
            ];
            
            if(clazz && clazz[perspectiveType]) {
                configArray.push(clazz);
            }
            else if(defaultClazz && defaultClazz[perspectiveType]) {
                configArray.push(defaultClazz);
            }
            else if(wild && wild[perspectiveType]){
                //No perspective under perpectiveName or default, but we do have a wildcard 
                //  --> just utilize the wildcard
                configArray.push(wild);
            }
            else {
                //Only auto-generate a perspective if we don't have one 
                // note: this will create and save a "default" perspective, but not one 
                configArray.push(autogenPerspective(boClass, perspectiveType));
            }
            
            configArray.push(displayOptions);
            
            return Q.all(configArray);
        })
        .then(function(configArray) {
            //dereference any "usePerspective" directives that may be present
            const promiseArray = [];
            
            for(let i=0; i < configArray.length-1; i++) {
                promiseArray.push(resolveUsePerspective(configArray[i], boClass, perspectiveName, perspectiveType, userId))
            }
            promiseArray.push(configArray.pop());
            
            return Q.all(promiseArray);
      })
      .spread((root,base,wild,clazz,displayOptions) => {
        
        root = root && root[perspectiveType];
        base = base && base[perspectiveType];
        wild = wild && wild[perspectiveType];
        clazz = clazz[perspectiveType];
        
        var merged = _.mergeWith(root||{}, base||{}, arrayMerger); //base onto root...
        wild && _.mergeWith(merged, wild, arrayMerger);  //wild onto merged base/root
        _.mergeWith(merged, clazz, arrayMerger); //clazz atop the whole thing
    
        //merge fieldCustomizations atop more general displayOptions
        _.mergeWith(displayOptions, merged.fieldCustomizations || {}, arrayMerger);
        merged.fieldCustomizations = displayOptions;
        
        cleanupAppends(merged);
        return merged;
      });
    
    
    };
    
    
    return exports;
}