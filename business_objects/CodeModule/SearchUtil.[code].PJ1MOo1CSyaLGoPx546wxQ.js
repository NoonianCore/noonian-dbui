function(db, _, Q) {
    const exports = {};
    
    
    const escapeRegexCharsMatcher = /[-[\]{}()*+?.,\\^$|#\s]/g;
    exports.escapeRegexChars = function(str) {
        return str && str.replace(escapeRegexCharsMatcher, '\\$&')
    };
    
    /**
     * Constructor for a QuerySequence object.
     * Queries className objects to find up to maxResults matches.
     * The intention is to have more strict queries early on to gather the best matches 
     * at the top, and progressively relax constraints to find more matches
     */
    const QuerySequence =
    exports.QuerySequence = function(className, maxResults, globalFilter, sort) {
        this.Model = db[className];
        
        if(!this.Model) {
            throw 'invalid BusinessObject class '+className;
        }
        
        this.queryList = [];
        
        this.maxResults = maxResults;
        this.globalFilter = globalFilter;
        this.sortClause = sort;
    };
    
    QuerySequence.prototype.sort = function(sort) {
        this.sortClause = sort;
    };
    
    QuerySequence.prototype.add = function(query) {
        this.queryList.push(query);
    };
    
    QuerySequence.prototype.getResults = function(queryIndex) {
        
        const moreResultsNeeded = this.maxResults - this.matchedIds.length;
        
        if(moreResultsNeeded <= 0) {
            return Q([]);
        }
        
        
        const queryComponents = [this.queryList[queryIndex]];
        
        if(this.matchedIds.length) {
            queryComponents.push({_id:{$nin:this.matchedIds}});
        }
        
        if(this.globalFilter) {
            queryComponents.push(this.globalFilter);
        }
        
        const fullQuery = queryComponents.length === 1 ? queryComponents[0] : {$and:queryComponents};
        
        var queryExec = this.Model.find(fullQuery);
        if(this.sortClause) {
            queryExec = queryExec.sort(this.sortClause);
        }
        
        return queryExec.limit(moreResultsNeeded).exec().then(matches=>{
            _.forEach(matches, m=>{
                this.matchedIds.push(m._id);
                
                this.results.push({
                    _disp:m._disp,
                    _id:m._id
                });
            });
        });
    };
    
    QuerySequence.prototype.exec = function() {
        
        const Model = this.Model;
        const queryList = this.queryList || []; 
        
        this.matchedIds = []; //Accumulate matches as we go down the list
        this.results = [];
        
        var promiseChain = Q(true);
        
        for(let i=0; i < this.queryList.length; i++) {
            promiseChain = promiseChain.then(
                this.getResults.bind(this, i)
            );
        }
        
        return promiseChain.then(()=>this.results);
    };
    
    
    return exports;
}