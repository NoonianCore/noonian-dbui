function(NoonWebService, DbuiAlert, $interval, $timeout, $rootScope) {
    
    const pkgMgrAction = {state:'dbui.sys_pkg_mgmt'};
    
    const pkgPickerItems = [];
    
    const menuItems = [
        {
            action:pkgMgrAction,
            label:'Package Manager',
            icon:'fas fa-box-open'
        },
        {isDivider:true},
        {
            label:'Building:',
            isOpen:true,
            submenu:pkgPickerItems
        }
    ];
    var curr = null;
    
    const setCurrentDisp = k=>{
        if(k === curr) return;
        curr = k;
        this.label = 'Building: '+(k || '(disabled)');
        pkgPickerItems.forEach(m=>{
            if(m.pkg === k) {
                m.icon  = 'fas fa-circle';
            }
            else {
                m.icon = 'far fa-circle';
            }
        });
    };
    
    const setCurrent = function(k) {
        if(k === curr) return;
        let savedIcon = this.icon;
        this.icon = 'fa-spinner fa-spin';
        
        return NoonWebService.call('sys/packageBuildConfig', {set:k}).then(pbc=>{
            setCurrentDisp(k);
            this.icon = savedIcon;
            DbuiAlert.success(k ? 'Currently building '+k : 'Package Building Disabled');
        },
        err=>{
            DbuiAlert.danger(err);
        });
    };
    
    const init = function() {
        
        NoonWebService.call('sys/packageBuildConfig').then(pbc=>{
            this.packageBuildConfig = pbc;
            console.log('packageBuildConfig', pbc);
            
            pkgPickerItems.splice(0, pkgPickerItems.length);
            this.action = null;
            this.submenu = menuItems;
            
            Object.keys(pbc).sort().forEach(k=>{
                if(k === '_enabled') return;
                
                pkgPickerItems.push({
                    pkg:k,
                    label:k,
                    action:{
                        fn:setCurrent.bind(this,k)
                    }
                })
            });
            
            if(pkgPickerItems.length) {
                pkgPickerItems.push({
                    pkg:false,
                    label:'Disable',
                    icon:'fa-times',
                    action:{
                        fn:setCurrent.bind(this,false)
                    }
                });
                setCurrentDisp.call(this, pbc._enabled);
                
                if(!this.checkerInterval) {
                    this.checkerInterval = $interval(()=>{
                        NoonWebService.call('sys/packageBuildConfig',{poll:true}).then(currKey=>setCurrentDisp(currKey));
                    }, 5000);
                }
                
            }
            else {
                // this.isHidden = true;
                this.label = 'Package Manager';
                this.submenu = null;
                this.action = pkgMgrAction;
            }
        });
        
        if(!this.refreshWatcher) {
            this.refreshWatcher = $rootScope.$on('dbui.refresh', (evt, params)=>{
            if(params && params.className === 'BusinessObjectPackage') {
                $timeout(init.bind(this));
            }
        });
        }
        
    }
    
    if(!this.packageBuildConfig) {
        init.call(this);
    }
    
    
    return menuItems;
}