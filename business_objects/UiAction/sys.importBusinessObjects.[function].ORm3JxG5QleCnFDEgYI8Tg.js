function ($rootScope, $uibModal, $http, BusinessObjectModelFactory, Dbui) {
    
    var modalInstance;
    var scope = $rootScope.$new(true);
    
    const templateHtml = '<div class="modal-header"><h3 class="modal-title">Import</h3></div><div class="modal-body"><i class="fa fa-spinner fa-spin" ng-show="waiting"></i><input ng-hide="waiting" type="file" id="importFile" accept=".json" onchange="angular.element(this).scope().fileChanged(this)"></div><div class="modal-footer"> <button class="btn btn-primary" type="button" ng-click="$dismiss()">Close</button></div>';
    
    
    scope.fileChanged = function(elem) {
        
        if(!elem || !elem.files || !elem.files.length) {
            scope.fileObj = scope.fileData = null;
            return;
        }
        
        const fileObj = scope.fileObj = elem.files[0];

        scope.fileData = {
          filename:fileObj.name,
          size:fileObj.size,
          type:fileObj.type
        };
        
        scope.uploadFile();

    };
    
    scope.uploadFile = function() {
        var fd = new FormData();
        fd.append('exportFile', scope.fileObj);

        var httpConfig = {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
        };

        var uploadWs = 'ws/sys/businessObjectImport';

        scope.waiting = true;
        $http.post(uploadWs, fd, httpConfig)
        .then(function(result) {
          scope.waiting = false;
          var d = result.data;
          console.log(d);
          alert('Imported :'+d.imported.length+'\nConflicts:'+d.conflicts.length);
          modalInstance.dismiss();
        },
        function(err) {
            scope.waiting = false;
            console.log(err);
            
        });
    };
    
    
    
    modalInstance = $uibModal.open({
        template:templateHtml,
        size:'sm',
        scope: scope
    });
    
    modalInstance.result.then(()=>{
        console.log('yay!');
    });
      
}