function(db, $q) {
    var objList = this.targetObj;
    var objId = null;
    if(!(objList instanceof Array)){
        objId = objList._id;
        objList = [objList];
    }
    
    if(!this.setter) {
        return console.error('UiAction not configured properly', this);
    }
    
    if(this.confirmMessage && !confirm(this.confirmMessage)) {
        return;
    }
    
    var promiseChain = $q.when(true);
    var saveObj = function() {return this.save()};
    _.forEach(objList, obj=>{
        _.assign(obj, this.setter);
        promiseChain = promiseChain.then(saveObj.bind(obj));
    });
    
    
    return promiseChain.then(()=>{ 
        var bcast =  {className:this.className};
        if(objId) {
            bcast.id = objId;
        }
        $rootScope.$broadcast('dbui.refresh', bcast);
        return {message:(this.successMessage||'Success')};
    });
    
    
}