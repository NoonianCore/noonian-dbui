function ($uibModal, $rootScope) {
    console.log('dbui.showExplainer', this);
    const lg = _.get(this, 'targetObj._bo_meta_data.field_labels');
    
    
    var scope = $rootScope.$new(true);
    scope.fieldLabel = _.get(lg, this.fieldName) || this.fieldName;
    scope.explainerText = _.get(lg, '_explainer.'+this.fieldName) || '(missing text)';
    
    
    $uibModal.open({
        templateUrl:'dbui/core/explainer-dialog.html',
        size:'sm',
        scope: scope
    });
    
}