function (db, queryParams, Q, _, SearchUtil, req, auth) {
    
    const className = queryParams.class_name;
    const searchTerm = queryParams.search_term;
    var limit = +queryParams.limit;
    
    var filter = queryParams.filter;
    
    if(!className || !searchTerm) {
        throw new Error('missing parameters');
    }
    
    if(filter) {
        filter = JSON.parse(filter);
    }
    
    if(isNaN(limit)) {
        limit = 10;
    }
    
    const Model = db[className];
    
    return auth.aggregateReadDacs(req, Model).then((dacObj, dbuiPrefs)=>{
        
        var dacCond = dacObj && dacObj.condition;
        
        if(dacCond) {
            if(filter) {
                filter = {$and:[dacCond, filter]};
            }
            else {
                filter = dacCond;
            }
        }
        

    
        const querySeq = new SearchUtil.QuerySequence(className, limit, filter);
        
        
        if(Model._bo_meta_data.type_desc_map._disp) {
            querySeq.sort({__disp:1});
            const escapedSearchTerm = SearchUtil.escapeRegexChars(searchTerm);
            
            querySeq.add({
                __disp:{$regex:'^'+escapedSearchTerm, $options:'i'}
            });
            querySeq.add({
                __disp:{$regex:escapedSearchTerm, $options:'i'}
            })
        }
        
        querySeq.add({$fulltextsearch:searchTerm});
        
        return querySeq.exec();
    });
        
    
}