function (queryParams, db, Q, invoker) {
    const className = queryParams.className;
    const id = queryParams._target_id;
    const fnName = queryParams._target_fn;
    
    delete queryParams._target_id;
    delete queryParams.className;
    delete queryParams._target_fn;
    
    return db[className].findOne({_id:id}).then(targetObj=>{
        if(!targetObj) {
            throw `Missing object ${className}.${id}`;
        }
        if(targetObj[fnName] instanceof Function) {
            return invoker.invokeInjected(targetObj[fnName], queryParams, targetObj);
        }
        else {
            throw `Missing function ${className}.${id}`;
        }
    });
}