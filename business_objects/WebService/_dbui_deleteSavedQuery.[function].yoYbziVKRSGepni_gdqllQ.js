function (queryParams, db, req, _) {
    var boClass = queryParams.class_name;
    var perspectiveName = queryParams.perspective;
    var title = queryParams.title;
    
    if(!boClass || !perspectiveName || !title) {
        throw new Error('Missing required parameters');
    }
    
    
    var currUserId = req.user._id;
    
    var configKey = 'sys.dbui.perspective.'+perspectiveName+'.'+boClass;
    return db.Config.findOne({key:configKey, user:currUserId}).then(function(cfg) {
        if(!cfg) {
            throw new Error('config not not found: '+configKey+' for user '+currUserId);
        }
        
        let queryList = _.get(cfg, 'value.list.savedQueries');
        if(!queryList || !queryList.length) {
            throw new Error('no query list for '+configKey+', user: '+currUserId);
        }
        let index = -1;
        queryList.forEach((entry, i)=>{
            if(entry.title === title) {
                index = i;
            }
        });
        
        if(index < 0) {
            throw new Error('missing saved query: '+title);            
        }
        
        queryList.splice(index, 1);
        cfg.markModified('value');
        return cfg.save();
    })
    .then(function(cfg) {
        return {result:'success', queryList:cfg.value.list.savedQueries};
    })
    ;
}