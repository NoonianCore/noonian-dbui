function (queryParams, db, req, auth, Q, _) {
    
    //TODO: check DACs for object classes 
    const id = queryParams.id;
    
    if(!id) {
        throw new Error('Missing required parameters');
    }

    db.DbuiObjectAlert.remove({_id:id});
    
}