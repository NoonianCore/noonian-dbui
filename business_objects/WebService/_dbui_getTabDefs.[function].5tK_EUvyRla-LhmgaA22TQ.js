function (queryParams, db, req, auth, _, Q, I18n) {
    var keys = queryParams.keys;
    var centralClass = queryParams.centralClass;
    
    // var user = req.user;
    
    if(!keys.length) {
        throw new Error("Missing required parameter");
    }
    
    
    let queryObj = {key:{$in:keys}};
    
    if(centralClass) {
        queryObj = {$or:[
            queryObj, 
            {
                show_in_dropdown:true,
                'central_classes._disp':centralClass
            }
        ]}
    }
    
    
    //TODO {rolespec:{$satsifiedBy:user.roles}}
    return Q.all([
        db.PivotPageTabDef.find(queryObj).lean(),
        auth.getCurrentUser(req)
    ])
    .spread((tdList, currUser)=>{
        
        const labelKeys = _.map(tdList, 'label');
        
        return I18n.aggregateLabelGroups(labelKeys, currUser).then(labelGroups=>{
            
            _.forEach(tdList, tabDef=>{
                var lg = labelGroups[tabDef.label];
                
                tabDef.tabTitle = lg && lg[tabDef.key] || tabDef.label;
                
                tabDef.hideCondition = tabDef.hide_condition;
                tabDef.tabId = tabDef.key;
                
            })
            
            return tdList;
        });
    });
}