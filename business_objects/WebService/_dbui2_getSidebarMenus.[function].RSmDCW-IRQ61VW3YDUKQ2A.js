function (req, config, db, auth, DbuiMenuBuilder, Q, _, I18n) {
    var configKey = 'dbui.menuConfig';
    var currUser;
    
    return auth.getCurrentUser(req).then(function(u) {
        currUser = u;
        
        var configQuery = {
            key:configKey,
            $or:[
                {rolespec:{$satisfiedBy:currUser.roles}},
                {user:currUser._id}
            ]
        };
        
        return db.Config.find(configQuery);
    })
    .then(function(matchingConfigs) {
        var matchedKeys = {};
        var menuKeys = []; 
        var menuPromises = [];
        
        _.forEach(matchingConfigs, function(c) {
            
            if(c && c.value) {
                _.forEach(c.value.sidebars, menuKey=>{
                    if(!matchedKeys[menuKey]) {
                        matchedKeys[menuKey] = true;
                        menuKeys.push(menuKey);
                        menuPromises.push(DbuiMenuBuilder.buildMenu(menuKey, currUser));
                    }
                });
            }
        });
        
        menuPromises.push(
            I18n.aggregateLabelGroups(menuKeys, currUser)
        );
        
        
        return Q.all(menuPromises).then(function(resultArr) {
            var result = {_primary:menuKeys[0]};
            for(var i=0; i < menuKeys.length; i++) {
                var menuKey = menuKeys[i];
                result[menuKey] = resultArr[i];
            }
            
            result._labels = resultArr.pop();
            
            return result;
        });
    })
    ;
    
    
}