function (queryParams, db, req, auth, Q, _) {
    
    const boClass = queryParams.class_name;
    const boId = queryParams.id;
    const keyFilter = queryParams.key_filter;
    
    if(!boClass || !boId) {
        throw new Error('Missing required parameters');
    }

    
    return auth.getCurrentUser(req).then(function(u) {
        const queryObj = {
            'objects._id':boId,
            $or:[
                {user:{$isEmpty:true}},
                {'user._id':u._id}
            ],
            roles:{$satisfiedBy:u.roles}
        };
        
        if(keyFilter) {
            queryObj.key = {$regex:keyFilter};
        }
        
        return db.DbuiObjectAlert.find(queryObj).sort({created_date:'asc'});
    });
    
}