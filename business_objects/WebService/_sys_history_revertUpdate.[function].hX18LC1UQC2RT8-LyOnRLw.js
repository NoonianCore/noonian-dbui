function(queryParams, db, Q, _, nodeRequire) {
    const updateLogId = queryParams.id;
    
    if(!updateLogId) {
        throw 'Missing required param(s)';
    }
    
    const diffTool = nodeRequire('../api/datasource/packaging/diffpatch');
    
    
    return db.UpdateLog.findOne({_id:updateLogId}).then(ul=>{
        
        if(!ul) {
            throw 'invalid UpdateLog id '+updateLogId;
        }
        
        return ul.revert(true).then(()=>{
            return {message:`Successfully reverted changes to ${ul.object_class} ${ul.object_id}`};
        });
    })
    ;
}