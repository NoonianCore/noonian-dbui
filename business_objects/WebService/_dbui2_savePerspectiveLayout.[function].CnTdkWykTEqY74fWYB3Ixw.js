function (db, queryParams, req, postBody, config, res) {
    var name = queryParams.name;
    const boClass = queryParams.class_name;
    const type = queryParams.type;
    const custom = (queryParams.custom === 'true');
    
    const userId = req.user._id;
    
    if(!boClass || !type || !postBody) {
        throw new Error("Missing required parameter");
    }
    
    
    if(!name) {
        name = 'default';
    }
    
    const configKey = `sys.dbui.perspective.${name}.${boClass}`;
    
    if(!custom) {
        let isAdmin = false;
        let roles = res.locals.user && res.locals.user.roles || [];
        for(var i=0; i < roles.length; i++) {
          if(roles[i] && roles[i]._id === 'FnQ_eBYITOSC8kJA4Zul5g') {
            isAdmin = true;
            break;
          }
        }
        if(!isAdmin) {
            throw new Error('Not authorized to update perspective');
        }
        
        return config.getParameter(configKey, {}).then(persp=>{
            
            const pt = persp[type] = persp[type] || {};
            
            pt.layout = postBody;
            
            return config.saveParameter(configKey, persp);
        })
        .then(()=>{
            let resultMsg = `Successfully saved ${type} perspective for ${boClass}: "${name}"`
            return {result:'success', message:resultMsg};
        })
        ;
    }
    else {
        //Saving customization
        return db.Config.findOne({key:configKey, 'user._id':userId}).then(result=>{
            const configObj = result || new db.Config({key:configKey, user:{_id:userId}});
            
            const persp = configObj.value = configObj.value || {};
            const pt = persp[type] = persp[type] || {};
            
            pt.layout = postBody;
            configObj.markModified('value');
            
            return configObj.save();
        })
        .then(()=>{
            let resultMsg = `Successfully saved customized ${type} perspective for ${boClass}: "${name}"`
            return {result:'success', message:resultMsg};
        });
    }
    
    
}