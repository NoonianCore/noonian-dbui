function(queryParams, db, _, Q) {
    return db.BusinessObjectDef.find({}).then(bodList=>{
        const ret = [];
        _.forEach(bodList, bod=>{
            _.forEach(bod.definition, (td, field)=>{
                var isArray = td instanceof Array;
                td = isArray ? td[0] : td;
                
                if(td.type === 'rolespec') {
                    ret.push({class:bod.class_name, field, td});
                }
                else if(td.type === 'reference' && td.ref_class === 'Role') {
                    ret.push({class:bod.class_name, field, td});
                }
                
            })
        });
        return ret;
    })
}