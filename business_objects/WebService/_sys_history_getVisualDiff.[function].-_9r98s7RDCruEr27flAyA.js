function(queryParams, db, Q, _, nodeRequire, res) {
    const updateLogId = queryParams.id;
    
    if(!updateLogId) {
        throw 'Missing required param(s)';
    }
    

    
    res.type('html');
    
    const diffTool = nodeRequire('../api/datasource/packaging/diffpatch');
    const jsondiffpatch = require('jsondiffpatch');
    
    return db.UpdateLog.findOne({_id:updateLogId}).then(ul=>{
        
        if(!ul) {
            throw 'invalid UpdateLog id '+updateLogId;
        }
        
        return ul.revert(false).then(targetObj=>{
            
            
            if(ul.revert_patch) {
                let cssPath = require('path').resolve('.', 'node_modules/jsondiffpatch/dist/formatters-styles/html.css');
                let css = require('fs').readFileSync(cssPath, 'UTF-8');
                let reverseDelta = diffTool.reverse(ul.revert_patch);
                
                let visualDiff = jsondiffpatch.formatters.html.format(reverseDelta, targetObj);
                return `<html><head><style type="text/css">${css}</style></head><body>${visualDiff}</body></html>`;
            }
            else {
                return 'No change was made on this update';
            }
            
        });
    });
}