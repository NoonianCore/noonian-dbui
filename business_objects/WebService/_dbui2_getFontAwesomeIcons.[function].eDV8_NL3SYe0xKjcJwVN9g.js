function(config, Q) {
    const fs = require('fs');
    const path = require('path');
    
    const instanceDir = config.serverConf.instanceDir;
    const cssPath = 'client/bower_components/fontawesome/css/all.css';
    
    const deferred = Q.defer();
    
    const regex = /\.(fa-[^:]+):before/;

    fs.readFile(path.join(instanceDir, cssPath), 'UTF-8', (err, cssContent)=>{
        
        if(err) {
            return deferred.reject(err);
        }
        let lines = cssContent.split('\n');
        let ret = [];
        
        lines.forEach(line=>{
            let result = regex.exec(line);
            if(result) {
                ret.push(result[1]);
            }
        })
        deferred.resolve(ret);
    });
    
    
    return deferred.promise;
}