function (req, config, db, auth, DbuiMenuBuilder, Q, _, I18n) {
    var configKey = 'dbui.menuConfig';
    var currUser;
    
    return auth.getCurrentUser(req).then(function(u) {
        currUser = u;
        
        var configQuery = {
            key:configKey,
            $or:[
                {rolespec:{$satisfiedBy:currUser.roles}},
                {user:currUser._id}
            ]
        };
        
        return db.Config.find(configQuery);
    })
    .then(function(matchingConfigs) {
        var matchedKeys = {};
        var menuKeys = []; 
        var menuPromises = [];
        
        _.forEach(matchingConfigs, function(c) {
            var menuKey = c && c.value && c.value.navbar;
            if(menuKey && !matchedKeys[menuKey]) {
                matchedKeys[menuKey] = true;
                menuPromises.push(DbuiMenuBuilder.buildMenu(menuKey, currUser));
            }
        });
        
        
        return Q.all(menuPromises).then(menus=>{
            var result = [];
            _.forEach(menus, m=>{
                result = result.concat(m);
            })
            return result;
        });
    })
    ;
    
    
}