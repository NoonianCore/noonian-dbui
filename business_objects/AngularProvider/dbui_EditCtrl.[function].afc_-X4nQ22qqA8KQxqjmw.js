function ($scope, $state, db, DbuiAlert, DbuiAction, NoonI18n, theObject, editPerspective, $rootScope, DbuiPerspectiveEditor, $timeout) {

    var className = $scope.boClass = $state.params.className;
    $scope.boId = $state.params.id;
    $scope.theObject = theObject;
    $scope.editPerspective = editPerspective;
    
    if(!theObject) {
        return;
    }
    
    const opts = editPerspective.options || {};
    var formStatus = $scope.formStatus = theObject._formStatus = {};
    
    const resetFormStatus = function() {
        formStatus.isDirty = false;
    }

    var boId = $scope.boId = theObject && theObject._id;
    
    var title; 
    
    if(boId) {
        title = 'Edit '+theObject._disp;
    }
    else {
        title = 'New '+className;
    }
    
    $scope.setPageTitle(title);
    
    $rootScope.watchFormStatus(formStatus);
    
    var onExit;
    
    var deRegisterStateChange = $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
        if(formStatus.isDirty && !confirm('Are you sure you want to navigate away without saving?')){
            event.preventDefault();
        }
        else {
            onExit();
        }
    });
    
    var deregisterRefreshWatch = $rootScope.$on('dbui.refresh', function(evt, params) {
        if(params && params.id === theObject._id) {
            theObject.reload().then(function() {
                $timeout(resetFormStatus);
            });
        }
    });
    
    onExit = function() {
        $rootScope.unwatchFormStatus(formStatus);
        deRegisterStateChange();
        deregisterRefreshWatch();
    };
    
    var specialActions;

    var saveTheObject = function() {
      console.log('saving object: ', $scope.theObject);
      specialActions.save.icon = 'fa-spinner fa-spin';
      if(opts.refreshOnSave) {
        $scope.refreshing = true;
      }
      $scope.theObject.save().then(
        function(result) {
          DbuiAlert.success('Successfully saved '+className+' "'+result._disp+'"');
          $scope.refreshing = null;
          resetFormStatus();
          specialActions.save.icon = specialActions.save.origIcon;
          
          if(!boId) {
            $state.go('dbui.edit', {className:className, id:result._id, perspective:editPerspective.name});
          }
        },
        function(err) {
          DbuiAlert.danger('Problem saving: '+err);
          specialActions.save.icon = specialActions.save.origIcon;
          $scope.refreshing = null;
          formStatus.isDirty = true;
        }

      );
    };

    var revert = function() {
      if(window.confirm('Are you sure?')) {
        theObject = $scope.theObject = db[className].findOne({_id:boId});
        
        //Need to re-bind the action invoker:
        $scope.invokeAction = DbuiAction.invokeAction.bind(DbuiAction, editPerspective, theObject);
      }
    };

    specialActions = {
      save:{
        id:'save',
        label:'Save',
        icon:'fa-save',
        origIcon:'fa-save',
        fn: saveTheObject
      },
      revert:{
        id:'revert',
        label:'Revert',
        icon:'fa-undo',
        fn: revert
      }
    };


    $scope.labels = NoonI18n.getBoLabelGroup(className);
    
    
    // function(perspectiveObj, contextBo, actionObj, argsObj)
    $scope.invokeAction = DbuiAction.invokeAction.bind(DbuiAction, editPerspective, theObject);
    
    if(editPerspective.actions) {
        $scope.actionList = DbuiAction.unaliasActionList(editPerspective.actions);
    }
    
    if(editPerspective.recordActions) {
        $scope.recordActionList = DbuiAction.unaliasActionList(editPerspective.recordActions, specialActions);
        DbuiAction.processActionVisibility($scope.recordActionList, theObject);
    }

    
    if(editPerspective.onLoadAction) {
        DbuiAction.invokeAction(editPerspective, theObject, editPerspective.onLoadAction);
    }
    
        
    if(editPerspective.allowEditLayout) {
        $scope.editLayout = function() {
            DbuiPerspectiveEditor.showLayoutEditorDialog(className, editPerspective, 'edit').then(result=>{
                
                if(result) {
                    editPerspective.layout = result;
                    //Refresh the object editor
                    $scope.refreshing = true;
                    $timeout(function() {
                        $scope.refreshing = null;
                    }, 100);
                }
            });
        };
    }

  }