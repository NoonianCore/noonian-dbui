function ($rootScope, $injector, $scope, $location, NoonAuth, NoonConfig,NoonWebService,DbuiAlert,DbuiAction) {
    
    $scope.loggedIn = false;
    $scope.isAdmin = false;
    
    $scope.appTitle = $rootScope.instanceName;
    
    $scope.isCollapsed = false;
    
    const processDynamicMenus = function(menu) {
        if(!menu) return;
        _.forEach(menu, menuItem=>{
            
            let dg = _.get(menuItem, 'submenu.definition_getter');
            
            if(dg) {
                try {
                    var fn;
                    eval('fn = '+dg);
                    menuItem.submenuGetter = fn;
                    menuItem.submenu = $injector.invoke(fn, menuItem);
                }
                catch(err) {
                    console.error(err);
                }
            }
            
            
            processDynamicMenus(menuItem.submenu);
        });
    };
    
    NoonAuth.onLogin( function(evt, userObj) {
        $scope.loggedIn = true;
        $scope.currentUser = userObj;
        $scope.isAdmin = userObj.isAdmin;
        
        NoonWebService.call('dbui2/getNavbarMenu').then(function(menu) {
            console.log('navbar menu:', menu);
            processDynamicMenus(menu);
            $scope.lefthandMenu = menu;
        });
        
    });


    NoonAuth.onLogout( function() {
        $scope.loggedIn = false;
        $scope.isAdmin = false;
    });
    
    
    $scope.toggleSubmenu = function(evt, item) {
        evt.stopPropagation(); //To prevent uib-dropdown from trashing the change
        item.isOpen = !item.isOpen;
    };

    $scope.logout = function() {
      NoonAuth.logout();
      window.location.reload();
    };

    $scope.invokeAction = function(action) {
        _.forEach($scope.lefthandMenu, m=>{m.isOpen=false});
        DbuiAction.invoke(action);
    };
    
    $scope.appTitleRightClick = function() {
        var myUrl = $location.absUrl();
        window.open(myUrl);
    };
    
    $scope.userProfile = function() {
        NoonConfig.getParameter('dbui.userProfileAction').then(
          function(action) {
            DbuiAction.invoke(action);
          },
          function(err) {
              console.log(err);
              DbuiAlert.danger("UserProfileAction action not properly configured. Be sure to set config parameter 'dbui.userProfileAction'")
          }
        );
    };
    
  }