function ($scope, $state) {
    
    $scope.editRepo = function(repo) {
        $state.go('dbui.edit', {
            perspective:'default',
            className:'RemotePackageRepository', 
            id:repo._id
        });
    };
    
    $scope.newRepo = function() {
        
        $state.go('dbui.create', {
            perspective:'default',
            className:'RemotePackageRepository'
        });
    };
    
    
      
}