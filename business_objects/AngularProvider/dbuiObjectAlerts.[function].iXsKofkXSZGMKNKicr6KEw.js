function (NoonWebService, DbuiAction) {
    
    return {
        templateUrl:'dbui2/core/helper/dbui_object_alerts.html',
        restrict: 'E',
        
        scope: {
            className: '@?',
            id: '@?',
            obj:'<?',
            keyFilter:'@'
        },
        
        
        controller: function($scope) {
            
            const dismissAction = function() {
                NoonWebService.call('dbui2/dismissObjectAlert', {id:this._id}).then(function() {
                    $scope.refreshAlertList();
                })
            }
            
            const refreshAlertList = 
            $scope.refreshAlertList = function() {
                // console.log('refresing dbuiObjectAlerts directive', $scope);
                
                $scope.invokeAction = DbuiAction.invokeAction.bind(DbuiAction, null, $scope.obj);
                
                var {className, id, obj, keyFilter} = $scope;
                
                if(!className || !id) {
                    if(!obj || !obj._id) return;
                    
                    id = obj._id;
                    className = obj._bo_meta_data.class_name;
                }
                
                
                NoonWebService.call('dbui2/getObjectAlerts', {id, class_name:className, keyFilter}).then(function(result) {
                    const alertList = $scope.alertList = result;
                    
                    _.forEach(alertList, function(a) {
                        if(a.actions && a.actions.length) { 
                            a.actionList = DbuiAction.unaliasActionList(a.actions, {
                                dismiss:{icon:'fa-times',tooltip:'dismiss', fn:dismissAction.bind(a)}
                            });
                            
                            if($scope.obj) {
                                DbuiAction.processActionVisibility(a.actionList, $scope.obj);
                            }
                            
                        }
                    });
                });
            };
            
            // refreshAlertList();
            $scope.$watch('className+id', refreshAlertList);
            $scope.$watch('obj');
            
        }
    };
}