function ($compile, $injector) {
  return {
      
    template:'<div></div>',
    
    restrict: 'E',
    scope: {
      centralObj: '=',  //Object being displayed (a model instance from datsource)
      pageConfig: '=',
      myTab: '=',
      initPromise:'=',
      //pivotApi: '='
    },
    
    link:function(scope, iElement, iAttributes) {
        const tabDef = scope.myTab;
        //Compile the tab's template html, set it as this directives DOM element:
        iElement.html($compile(tabDef.template)(scope));
    },
    
    controller:function($scope, $parse) {
        const tabDef = $scope.myTab;
        
        $scope.perspectiveName = $scope.pageConfig.name;
        $scope.centralClass = $scope.centralObj._bo_meta_data.class_name;
        
        //Attach the PivotPageTabDef controller:
        if(tabDef.controller) {
            const fnString = tabDef.controller;
            try {
                var toCall;
                eval('toCall = '+fnString);
                if(typeof toCall === 'function') {
                    return $injector.invoke(toCall, this, {$scope})
                }
            }
            catch(err){
                console.error('bad controller for PivotPageTabDef', tabDef, err);
            }
        }
    }
  };
}