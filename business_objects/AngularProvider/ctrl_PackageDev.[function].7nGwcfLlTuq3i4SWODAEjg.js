function ($scope, $rootScope, db, Dbui, NoonWebService, DbuiAlert) {
    
    $scope.newDev = function() {
        
        return Dbui.getPerspective('new_dev', 'BusinessObjectPackage', 'edit').then(p=>{
            $scope.pkgPerspective = p;
            $scope.newPackage = new db.BusinessObjectPackage({
                version:'0.0.0',
                build_config: {
                    exclude:[
                      'User',
                      'BusinessObjectPackage',
                      {
                        class_name:'Config',
                        condition: {
                            key:{
                                $contains: 'sys.user.preferences|sys.packageBuilding'
                            }
                        }
                      }
                    ]
                },
                dependencies:{
                    npm:{},
                    bower:{},
                    noonian:{}
                }
            }); 
            
            console.log('pkgPerspective', p, $scope.newPackage);
        });
    };
    
    $scope.loadDev = function(pkgMeta) {
        console.log(pkgMeta);
        if(confirm('Are you sure you wish to load from directory '+pkgMeta.subdirectory+'?')) {
            $scope.loadingPkg = pkgMeta.key;
            NoonWebService.call('/pkg/installPackage',{local_subdir:pkgMeta.subdirectory}).then(r=>{
                DbuiAlert.success(r.message);
                return NoonWebService.call('sys/packageBuildConfig',{set:pkgMeta.key}).then(wsResult=>{
                    DbuiAlert.info('Enabled build for '+wsResult._enabled);
                    $scope.loadingPkg = null;
                    $rootScope.$broadcast('dbui.refresh', {className:'BusinessObjectPackage'});
                },
                err=>{
                    DbuiAlert.danger('Problem setting packageBuildConfig (see log)');
                    console.error(err);
                });
            },
            err=>{
                console.error(err);
                DbuiAlert.danger(err);
            });
        }
    };
    
    $scope.createPkg = function() {
        $scope.creating = true;
        return $scope.newPackage.save().then(()=>{
            return NoonWebService.call('sys/packageBuildConfig',{set:$scope.newPackage.key}).then(wsResult=>{
                console.log('buildConfig', wsResult);
                DbuiAlert.info('Enabled build for '+wsResult._enabled);
                $scope.newPackage = $scope.creating = null;
                $rootScope.$broadcast('dbui.refresh', {className:'BusinessObjectPackage'});
            });
        },
        err=>{
            console.error(err);
            DbuiAlert.danger('Problem saving (see log)');
        });
    };
    
      
}