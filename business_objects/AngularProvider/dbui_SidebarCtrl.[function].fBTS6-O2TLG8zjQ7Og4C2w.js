function ($scope, NoonAction, NoonWebService, DbuiUserPrefs, $timeout, $rootScope, $window) {
    
    const AUTO_MINIMIZE_WIDTH = 768;
    var sidebarMenuMap;  //key->menu obj

    NoonWebService.call('dbui2/getSidebarMenus').then(function(menuMap) {
        sidebarMenuMap = menuMap;
        
        var pinned = DbuiUserPrefs.getParameter('pinnedSidebar');
        console.log('pinned', pinned);
        var pinnedKey = $scope.pinnedSidebar = pinned && pinned.key;
        var currMenuKey = $scope.currMenuKey = menuMap[pinnedKey] ? pinnedKey : menuMap._primary;
        
        var sm = $rootScope.sidebarMenu = menuMap[currMenuKey];
        
        var labels = menuMap._labels;
        var menuList = [];
        _.forEach(menuMap, function(menuObj, menuKey) {
            // console.log('menuObj', menuObj);
            if(menuKey.indexOf('_') === 0) return;
            menuObj.key = menuKey;
            menuObj.label = labels[menuKey] || menuKey;
            menuList.push(menuObj);
            // menuList.push({
            //     key:menuKey,
            //     label:labels[menuKey] || menuKey
            // });
            _.forEach(menuObj, m=>{
                _.forEach(m.submenu, s=>{
                    s.parent = m;
                });
            });
        });
        $scope.sidebarMenuList = _.sortBy(menuList, 'label');
        
        if(pinned && pinned.isOpen) {
            for(let i=0; i < pinned.isOpen.length; i++) {
                sm[i] && (sm[i].isOpen = pinned.isOpen[i]);
            }
        }
    });
    
    
    const setSidebarMinStatus = function(val) {
        // console.log('setSidebarMinStatus', val);
        val = !!val;
        const ls = $rootScope.layoutStatus;
        var currStatus = ls.sidebarMinimized;
        if(val !== currStatus) {
            ls.sidebarMinimized = val;
            $rootScope.$broadcast('dbui.notify.sidebarMinimizedStatusChange', val);
        }
    };
    
    setSidebarMinStatus($window.innerWidth < AUTO_MINIMIZE_WIDTH);
    
    $rootScope.$on('dbui.cmd.minimizeSidebar', setSidebarMinStatus.bind(null, true));
    $rootScope.$on('dbui.cmd.unminimizeSidebar', setSidebarMinStatus.bind(null, false));
    
    $rootScope.$on('dbui.cmd.setSidebar', function(evt, val) {
        console.log('dbui.cmd.setSidebar', val);
        var m = sidebarMenuMap[val];
        if(m) {
            $rootScope.sidebarMenu = m;
        }
    });
    
    angular.element($window).on('resize', function() {
        $scope.$apply(function() {
            if($window.innerWidth < AUTO_MINIMIZE_WIDTH) {
                setSidebarMinStatus(true);
            }
        });
    });
    
    var scrollbarVisible = $rootScope.checkLeftScrollbarVisible;
    

    $scope.toggleConfigMenu = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.configMenuOpen = !$scope.configMenuOpen;
    };
    
    $scope.toggleSidebarSelectorMenu = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.sidebarSelectorOpen = !$scope.sidebarSelectorOpen;
    }
    
    
    $scope.toggleMinimized = function() {
        setSidebarMinStatus(!$rootScope.layoutStatus.sidebarMinimized);
    };
    
    $scope.switchMenu = function(menu) {
        $rootScope.sidebarMenu = menu;
    };
    
    $scope.togglePinned = function() {
        let sm = $rootScope.sidebarMenu;
        let toSetKey = $scope.pinnedSidebar === sm.key ? null : sm.key;
        console.log('togglePinned', toSetKey, sm);
        $scope.pinnedSidebar = toSetKey;
        
        let isOpen = [];
        _.forEach(sm, menu=>{
            isOpen.push(menu.isOpen);
        });
        
        DbuiUserPrefs.setParameter('pinnedSidebar', {key:toSetKey, isOpen});
        
    };
    
    $scope.selectItem = function(item) {
        if(item.submenu && !item.hovering) {
            //SPECIAL LOGIC FOR MOBILE 
            //if menu item was selected from this menu before, the "cursor" lingers and never exits the element...
            item.hovering = true;
        }
        else if(item.submenu) {
            console.log('selected item w/ submenu, hovering='+item.hovering);
            item.isOpen = !item.isOpen;
            $timeout(scrollbarVisible, 1);
        }
        else if(item.action) {
            NoonAction.invoke(item.action);
            if($scope.layoutStatus.sidebarMinimized) {
                //When a menu item is selected, force menu popouts to disappear out from under cursor
                item.parent.hovering = false;
            }
        }
        else {
            console.error('menu item has no action ', item);
        }
    };
    
    $scope.collapseAll = function() {
        _.forEach($scope.sidebarMenu, function(m) {
            if(m.isOpen) m.isOpen = false;
        });
        $timeout(scrollbarVisible, 1);
    };
    
    $scope.expandAll = function() {
        _.forEach($scope.sidebarMenu, function(m) {
            m.isOpen = true;
        });
        $timeout(scrollbarVisible, 1);
    };
    
    
    //TODO: persist menu state on page unload (cookie?)
    // DbuiUserPrefs.setParameter('selected_sidebar_menu', key);
    
}