function ($timeout, $q, db, NoonI18n, DbuiAction, Dbui, DbuiPerspectiveEditor, $stateParams, DbuiFieldType, DbuiAlert) {
  return {
    templateUrl: 'dbui2/reusable/core/query_builder_panel.html',
    restrict: 'E',
    scope: {
      boClass: '<',
      perspective: '<',
      onClear: '&',
      applyCallback:'&'
    },
    
    
    controller:function($scope, NoonWebService) {
        
        var className = $scope.boClass;
        var perspective = $scope.perspective;
        
        var BoModel = db[className];
        $scope.objectMetaData = BoModel._bo_meta_data;
        $scope.typeDescMap = BoModel._bo_meta_data.type_desc_map;
        
        if(perspective.filter) {
            $scope.baseQuery = perspective._originalFilterDescription;
        }
        
        /**
         * @private
         * Create a "stub" query for typeDescMap
         */
        var createStubQuery = function() {
            var typeDescMap = $scope.typeDescMap;
            var fieldName = $scope.perspective.fields[0];
            var td = $scope.typeDescMap[fieldName];
            
            
            var stub = {};
            
            var opList = DbuiFieldType.getOpList(td);
            if(opList && opList.length) {
                stub[fieldName] = {};
                stub[fieldName][opList[0].op] = '';
                return stub;
            }
            else {
                //The passed-in field isn't searchable; so we'll pick another:
                for(var field in typeDescMap) {
                    td = typeDescMap[field];
                    opList = DbuiFieldType.getOpList(td);
                    if(opList && opList.length) {
                        stub[field] = {};
                        stub[field][opList[0].op] = '';
                        return stub;   
                    }
                }
            }
        };
        
        if(!perspective.extraFilter) {
            perspective.extraFilter = createStubQuery();
        }
        
        if($scope.perspective.showingRaw === undefined) {
            $scope.perspective.showingRaw = false;
        }
        
        $scope.showQueryBuilder = function() {
            $scope.perspective.showingRaw = false;
        };
        $scope.showRaw = function() {
            $scope.perspective.showingRaw = true;
        };
        
        $scope.execSearch = function() {
            $scope.applyCallback();
        };
        
        $scope.clear = function() {
            perspective.extraFilter = null;
            $scope.onClear();
        };
        
        $scope.saveQuery = function() {
            //Get a name for the query
            //is it for me or for everyone?
            //append it to perspective.savedQueries
            //save the perspective
            Dbui.showDialog(
                'Save Query', 
                'Please provide a description for the saved query.', 
                {name:{type:'string'},save_columns:{type:'boolean'}}, 
                {layout:['name', 'save_columns']}, 
                {},
                {name:'Name', save_columns:'Save Columns?'}
            ).then(function(result) {
                
                var wsParams = {
                    title:result.name,
                    theQuery:perspective.extraFilter,
                    perspective:perspective.name,
                    class_name:className
                };
                if(result.save_columns) {
                    wsParams.fields = perspective.fields;
                }
                
                NoonWebService.call('dbui/saveQuery', wsParams).then(function(result) {
                    // console.log(result);
                    perspective.savedQueries = result.queryList;
                });
            });
        };
        
        $scope.loadQuery = function(sq) {
            if(!$scope.loadedQuery) {
                $scope.beforeLoaded = perspective.extraFilter;
            }
            $scope.loadedQuery = sq;
            perspective.extraFilter = _.cloneDeep(sq.query);
            if(sq.fields) {
                perspective.fields = _.clone(sq.fields);
            }
            $scope.applyCallback();
        };
        
        $scope.dismissLoadedQuery = function() {
            perspective.extraFilter = $scope.beforeLoaded;
            $scope.loadedQuery = $scope.beforeLoaded = null;
            $scope.applyCallback();
        };
        $scope.deleteLoadedQuery = function() {
            if(confirm('Are you sure you want to remove: "'+$scope.loadedQuery.title+'"?')) {
                var wsParams = {
                    title:$scope.loadedQuery.title,
                    perspective:perspective.name,
                    class_name:className
                };
                
                NoonWebService.call('dbui/deleteSavedQuery', wsParams).then(function(result) {
                    perspective.savedQueries = result.queryList;
                    $scope.dismissLoadedQuery();
                },err=>{
                    console.log(err);
                    alert('an error occurred (see log)');
                });
            }
        }
    } // end controller definition
  }
}