function () {
    return {
        template:'<span class="fake-checkbox" ng-click="clicked()"><i class="fa-2x" ng-class="cboxClass"></i></span>',
        
        restrict: 'E',
        
        // require:'ngModel',
        
        scope: {
            // elemId:'@',
            bindValue:'='
        },
        
        
        link: function(scope, iElement, iAttributes) {
            
            var elem = $(iElement);
            var typeToState = {
                mouseenter:true,
                mouseleave:false
            };
            
            var setHoverState = function(evtType) {
                scope.hovering = typeToState[this.type];
            };
            
            elem.hover(function(evt){
                scope.$apply(setHoverState.bind(evt));
            });
            
            scope.$watch('bindValue+hovering', ()=>{
                var val = scope.bindValue;
                if(scope.hovering) {
                    scope.cboxClass = val ? 'fas fa-check-square' : 'fas fa-square';
                }
                else {
                    scope.cboxClass = val ? 'far fa-check-square' : 'far fa-square';
                }
            });
            
            scope.clicked = function() {
                scope.bindValue = !scope.bindValue;
            };
    
            // //1. Wire up converter for ng-model object (string)--> internal $viewValue representation (date)
            // ngModel.$formatters.push(function(modelValue) {
            //     if(modelValue) {
            //         return {value:new Date(moment(modelValue).format())};
            //     }
            //     return {value:null};
            // });
            
            // //2. Wire up converter for internal $viewValue representation (date) --> ng-model object (string)
            // ngModel.$parsers.push(function(viewValue) {
            //     if(viewValue && viewValue.value instanceof Date) {
            //         return moment(viewValue.value).format('YYYY-MM-DD');
            //     }
                
            //     return null;
            // });
            
            // //3. Wire up trigger for scope object --> $viewValue
            // scope.$watch('binding', function() {
            //     //must *replace* the viewValue object in order for change to propogate to ng-model!
            //     if(scope.binding && !angular.equals(ngModel.$viewValue, scope.binding)) {
            //         ngModel.$setViewValue({value:scope.binding.value});
            //         if(scope.binding.value) {
            //             textBox.css('background-color','');
            //         }
            //     }
                
            // }, 
            // true); 
            
            // //4. Wire up callback for $viewValue update --> scope object
            // ngModel.$render = function() {
            //     if(!scope.binding) {
            //         scope.binding = {};
            //     }
            //     if(!angular.equals(ngModel.$viewValue, scope.binding)) {
            //         scope.binding.value = ngModel.$viewValue.value;
            //     }
            // };
            
            
        }
      
    };
  }