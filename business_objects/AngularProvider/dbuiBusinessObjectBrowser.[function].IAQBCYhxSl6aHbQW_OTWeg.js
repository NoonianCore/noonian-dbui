function ($timeout, $q, db, NoonI18n, DbuiAction, NoonConfig,  Dbui, DbuiPerspectiveEditor, $stateParams, DbuiFieldType, DbuiAlert, $rootScope) {
  return {
    templateUrl: 'dbui2/reusable/core/object_browser.html',
    restrict: 'E',
    scope: {
      boClass: '<',
      perspective: '<',
      enableSearch: '<?',
      enableQuery: '<?'
    },
    
    
    controller:function($scope, NoonWebService) {
        
        var specialTableActions = {
            edit_perspective:{
                label:'Edit Columns',
                icon:'fa-list-alt',
                fn: function(args) {
                    return DbuiPerspectiveEditor.showColumnEditorDialog($scope.boClass, $scope.perspective).then($scope.loadData);
                }
            },
            refresh_data:{
                label:'Refresh Data',
                icon:'fa-sync',
                fn: function(args) {
                    $scope.loadData();
                }
            },
            
            multi_delete:{
                label:'Delete',
                icon:'fa-times-circle',
                id:'multi_delete',
                fn: function(args) {
                    var arr = args && args.targetObj;
                    var className = args && args.className;
                    if(!className || !db[className]) {
                        return DbuiAlert.danger('Invalid Class Name: '+className);
                    }
                    if(arr && arr.length) {
                        if(window.confirm('Are you sure you want to delete these '+arr.length+' items?')) {
                            var query = {_id:{$in:_.pluck(arr, '_id')}};
                            return db[className].remove(query).then(function(result){
                                if(result.result === 'success') {
                                    DbuiAlert.success('Deleted '+result.nRemoved+' '+className+' Records');
                                    $scope.loadData();
                                }
                                else {
                                    console.log(result);
                                    DbuiAlert.danger(result);
                                }
                                
                            },
                            function(err) {
                                DbuiAlert.danger(err);
                            }
                            )
                        }
                    }
                    else {
                        DbuiAlert.danger('No items selected');
                    }
                }
            }
        };
        
        $rootScope.$on('dbui.refresh', function(evt, params) {
            if(params && params.className === $scope.boClass) {
                $scope.loadData()
            }
        });
        
        
        var className = $scope.boClass;
        var perspective = $scope.perspective;
        
        var BoModel = db[className];
        $scope.objectMetaData = BoModel._bo_meta_data;
        $scope.typeDescMap = BoModel._bo_meta_data.type_desc_map;
        $scope.labels = NoonI18n.getBoLabelGroup(className);
        
        
        $scope.dataArray = [];
        
        
        if(!perspective.sort) {
            perspective.sort = {};
        }
        
        
        const defaultPageSizes = [5, 10, 25, 50, 100, 500];
        const defaultPageSize = 10;
        
        if(!perspective.pageState) {
            
            perspective.pageState = {
                current:1,
                pageSize:defaultPageSize,
                totalRecords:0
            };
            
            var pagingSpec = Dbui.getPreferenceValue('defaultPaging');
            if(pagingSpec) {
                perspective.pageState.pageSize = pagingSpec.defaultSize || defaultPageSize;
                $scope.pageSizes = perspective.pageState.pageSizes = pagingSpec.sizes || defaultPageSizes;
            }
            else {
                console.error('missing dbui preference for "defaultPaging"')
                perspective.pageState.pageSize = defaultPageSize;
                $scope.pageSizes = defaultPageSizes;
            }
        }
        else {
             $scope.pageSizes = perspective.pageState.pageSizes;
        }
        
        $scope.pageState = perspective.pageState;
        
        
        var searchPromise = null;
        var loadData = $scope.loadData = function() {
            
            if(searchPromise) {
                $timeout.cancel(searchPromise);
                searchPromise = null;
            }
            
            var pageSize = perspective.pageState.pageSize;
            var currPage = perspective.pageState.current;  //Bound to paginator
        
            var sort = perspective.sort;
            
            var queryDef = perspective.getEffectiveQuery();
            var selectObj = {}; //aka projection
            
            //Ask only for the fields we're showing
            for(var i=0; i <  perspective.fields.length; i++) {
                var f = perspective.fields[i];
                selectObj[f] = 1;
            }
        
            //Query options: limit, skip, sort, group-by
            var queryOpts = {limit:pageSize};
            if(currPage > 1) {
                queryOpts.skip = (currPage-1)*pageSize;
            }
            
            if(sort) {
                queryOpts.sort = sort;
            }
            
            if(perspective.groupBy) {
                queryOpts.groupBy = perspective.groupBy;
            }
            
            var stringifyPromise = queryDef ? Dbui.stringifyQueryClause(queryDef, className) : $q.resolve('');
            
            //Do the query!
            $scope.dataLoading = true;
            var resultList = BoModel.find(queryDef, selectObj, queryOpts);
            
            $scope.dataArray = resultList;
            $q.all([resultList.$promise, $scope.labels.$promise, stringifyPromise])
            .then( function(resultArr) {
                $scope.dataLoading = false;
                
                var result = resultArr[0];
                if(perspective.groupBy) {
                    var groupByField = perspective.groupBy;
                    var dataArray = [];
                    for(var i =0; i < result.length; i++) {
                        result[i].__group_header = true;
                        result[i][groupByField] = result[i].group[0][groupByField]; //copy the full value from the first member of the group
                        dataArray.push(result[i]);
                        
                        for(var j=0; j < result[i].group.length; j++) {
                            dataArray.push(result[i].group[j]);
                        }
                    }
                    
                    $scope.dataArray = dataArray;
                }
                
                $scope.filterDescription = resultArr[2] || 'ALL records';
                
                if(!perspective._originalFilterDescription) {
                    perspective._originalFilterDescription = $scope.filterDescription;
                }   
                
                var pageState = perspective.pageState;
                pageState.totalRecords = result.nMatched; //result._meta.nMatched;
                pageState.totalPages = Math.ceil(pageState.totalRecords/pageState.pageSize);
                pageState.rangeStart = pageState.pageSize * (pageState.current - 1) + 1;
                
                var rangeEnd = pageState.rangeStart + pageState.pageSize - 1;
                if(rangeEnd > pageState.totalRecords) {
                    pageState.rangeEnd = pageState.totalRecords;
                }
                else {
                    pageState.rangeEnd = rangeEnd;
                }
            },
            function(err) {
                $scope.dataLoading=false;
                console.error('DATA RETRIEVE ERROR:', err);
                DbuiAlert.danger('Error retrieving data (see log) ');
            });
        };
        
        $scope.pageChanged = loadData; //Called by paginator event
        
        $scope.searchStringChanged = function() {
            if(searchPromise) {
                $timeout.cancel(searchPromise);
            }
            
            //execute search half a second after search string stops changing
            var ss = perspective.searchString;
            
            if(ss != null && (ss.length >= 3 || ss === '')) {
                searchPromise = $timeout(loadData, 500);
            } 
        };
        
        $scope.toggleRegex = function() {
            perspective.searchRegex = !perspective.searchRegex;
            $scope.searchStringChanged();
        }
        
        $scope.querybuilderClosed = function() {
            perspective.isAdvancedSearch = false;
            loadData();
        };
        
        $scope.toggleAdvancedSearch = function() {
            let enable = perspective.isAdvancedSearch = !perspective.isAdvancedSearch;
            
            if(enable) {
                perspective.searchString = '';
                
                if(!perspective.extraFilter && perspective._stashedFilter) {
                    perspective.extraFilter = perspective._stashedFilter;
                }
            }
            else {
                perspective._stashedFilter = perspective.extraFilter;
                perspective.extraFilter = null;
                loadData();
            }
        };
        
        $scope.execSearch = function() {
            loadData();
        };
        $scope.clearTextSearch = function() {
            perspective.searchString = '';
            loadData();
        };
        
        $scope.keyPressed = function(evt) {
            if(evt.which === 13) {
                loadData();
            }
        };
        
        $scope.loadQuery = function(q) {
            perspective.extraFilter = _.cloneDeep(q);
            loadData()
        };
        
        //The config object for the dbui-object-table:
        $scope.objectTableConfig = {
            cellEdit: false,
            onSort: loadData,
            aliasActions:specialTableActions
        };
        
        //Do the initial data load:
        loadData();
        

    } // end controller definition
  }
}