function (NoonWebService, NoonI18n, db, $q) {
    var THIS = this;
    
    //Map fieldtype name to appropriate FieldTypeUiSpec object
    var viewSpecs = {};
    var editSpecs = {};
    var viewArrSpecs = {};
    var editArrSpecs = {};
    
    var specMaps = {view:viewSpecs, edit:editSpecs};
    var arrSpecMaps = {view:viewArrSpecs, edit:editArrSpecs};
    
    var queryOpLabels;
    var queryOpMap = {}; //Map fieldtype name to list of queryop
    
    var cachePromises = {}; //keep track of classes for whom we've called cache()
    
    /**
     * DbuiFieldType.init
     * 
     */
    this.init = function() {
        console.log('initializing DbuiFieldType'); 
        
        //Pull in queryop metadata
        return NoonWebService.call('dbui/getQueryOpMetadata').then(function(result) {
            
            _.forEach(result, function(editorSpec) {
                _.forEach(editorSpec.types, function(typeName) {
                   queryOpMap[typeName] = editorSpec.queryops; 
                });
            });
            
        });
        
    };
    
    /**
     * 
     * @private
     */
    var getTypeName = function(typeDesc) {
        if(typeDesc instanceof Array) {
            return typeDesc[0].type+'[]';
        }
        else if(typeDesc) {
            return typeDesc.type;
        }
    };
    
    var applicableWildcard = function(typeDesc) {
        return typeDesc instanceof Array ? '*[]' : '*';
    };
    
    /**
     * DbuiFieldType.cacheTypeInfo
     * call getFieldTypeMetadata to cache templates/controllers/etc. for a fieldtype or fieldtypes of a specific class
     * @private
     */
    var cacheTypeInfo = function (wsParams, viewEdit) {
        // console.log('cacheTypeInfo', wsParams, viewEdit);
        return NoonWebService.call('dbui/getFieldTypeMetadata', wsParams).then(function(resultObj) {
            
            _.forEach(resultObj.specObjects, function(specObj) {
                var typeList;
                if(specObj.fieldtypes && specObj.fieldtypes.length) {
                    typeList = specObj.fieldtypes;
                }
                else {
                    typeList = ['*'];
                }
                
                var specMap;
                if(specObj.for_array) {
                    specMap = arrSpecMaps[viewEdit];
                }
                else {
                    specMap = specMaps[viewEdit];
                }
                
                _.forEach(typeList, function(typeName) {
                    // console.log('saving template for ', typeName);
                    if(!specMap[typeName]) {
                        specMap[typeName] = {};
                    }
                    if(specObj.key.indexOf('dbui.core') === 0) {
                        specMap[typeName].default = specObj;
                    }
                    else {
                        specMap[typeName][specObj.key] = specObj;
                    }
                });
            });
            
        },
        function(err) {
            console.error('Bad result for cacheTypeInfo ', wsParams, err);
        });
    };
    
    /**
     * Point type name to wildcard spec if type-specific spec isn't cached
     */
    var setCacheWildcardIfNeeded = function(typeDesc, viewEdit) {
        // console.log('setting wildcard for typeDesc', typeDesc, viewEdit);
        var typeName;
        var specMap;
        if(typeDesc instanceof Array) {
            specMap = arrSpecMaps[viewEdit];
            typeName = typeDesc[0].type;
        }
        else {
            specMap = specMaps[viewEdit];
            typeName = typeDesc.type;
        }
        
        if(!specMap[typeName] && specMap['*']) {
            specMap[typeName] = specMap['*'];
        }
        if(specMap[typeName] && !specMap[typeName].default && specMap['*'] && specMap['*'].default) {
            specMap[typeName].default =  specMap['*'].default;
        }
    };
    
    /** 
     * DbuiFieldType.cacheTypeInfoForFieldtype
     * Retrieves and caches FieldTypeUiSpec data for fields of specified fieldtype
     */
    var cacheTypeInfoForFieldtype = function(typeDesc, viewEdit) {
        var typeName = getTypeName(typeDesc);
        var promiseCacheKey = typeName+'|'+viewEdit;
        
        if(!cachePromises[promiseCacheKey]) {
            cachePromises[promiseCacheKey] = cacheTypeInfo( {field_type:typeName,view_or_edit:viewEdit}, viewEdit )
                .then(setCacheWildcardIfNeeded.bind(null, typeDesc, viewEdit));
        }
        
        return cachePromises[promiseCacheKey];
    };
    
    /** 
     * DbuiFieldType.cacheTypeInfoForClass
     * Retrieves and caches FieldTypeUiSpec data for fields of specified class
     */
    this.cacheTypeInfoForClass = function(className) {
        if(!cachePromises[className]) {
            cachePromises[className] = cacheTypeInfo({class_name:className,view_or_edit:'view'}, 'view')
                .then(cacheTypeInfo.bind(null, {class_name:className,view_or_edit:'edit'}, 'edit'))
                .then(function() {
                    var typeDescMap = db[className]._bo_meta_data.type_desc_map;
                    _.forEach(typeDescMap, function(typeDesc) {
                        setCacheWildcardIfNeeded(typeDesc, 'view');
                        setCacheWildcardIfNeeded(typeDesc, 'edit');
                    });
                });
        }
        return cachePromises[className];
    };
    
    
    var getSpecFromCache = function(typeDesc, viewEdit) {
        var typeName;
        var specMap;
        if(typeDesc instanceof Array) {
            specMap = arrSpecMaps[viewEdit];
            typeName = typeDesc[0].type;
        }
        else {
            specMap = specMaps[viewEdit];
            typeName = typeDesc.type;
        }
        
        if(specMap[typeName]) {
           return specMap[typeName];
        }
    };
    
    /**
     * DbuiFieldType.getSpec
     */
    this.getSpec = function(typeDesc, viewEdit, fieldCustomizations) {
        var fc = fieldCustomizations || {};
        var specKey = fc.uiSpec || 'default';
        
        
        var specObj = getSpecFromCache(typeDesc, viewEdit);
        if(specObj) {
            return $q.resolve(specObj[specKey]);
        }
        
        //Not cached, retrieve it from server:
        return cacheTypeInfoForFieldtype(typeDesc, viewEdit).then(function() {
            return getSpecFromCache(typeDesc, viewEdit)[specKey];
        });
        
    };
    
    
    /**
     * 
     */
    var getOpList =
    this.getOpList = function(typeDesc) {
        var typeName = getTypeName(typeDesc);
        if(queryOpMap[typeName]) {
            return queryOpMap[typeName];
        }
        else {
            var wc = applicableWildcard(typeDesc);
            if(queryOpMap[wc]) {
                return queryOpMap[wc];
            }
        }
    };
    
    /**
     * 
     */
    this.getOpInfo = function(typeDesc, opName) {
        var opInfoList = getOpList(typeDesc);
        for(var i=0; i < opInfoList.length; i++) {
            if(opName === opInfoList[i].op) {
                return opInfoList[i];
            }
        }
    };
    
    
    const getSubfieldItems = function(boLabelGroup) {
        
        const ret = [];
        
        var namePrefix = this.fieldName+'.';
        
        var subfieldNames = this.filterSubfields || Object.keys(this.subTypeDescMap);
        
        for(var i=0; i < subfieldNames.length; i++) {
            var subfield = subfieldNames[i];
            var qualifiedSubfield = namePrefix+subfield;
            var fi = {
                fieldName:qualifiedSubfield,
                fieldLabel: boLabelGroup.getAbbreviatedLabel(qualifiedSubfield, true),
                td:this.subTypeDescMap[subfield]
            };
            ret.push(fi);
        }
        return ret;
    };

    var expandPlaceholder = function(fieldList) {
        
        var toSplice = this.parent.getSubfieldItems();
        
        var myPos;
        for(myPos=0; myPos < fieldList.length ; myPos++) {
          if(fieldList[myPos] === this)
            break;
        }
        fieldList.splice(myPos, 1);
        toSplice.forEach(fi=>fieldList.splice(myPos, 0, fi));
    };

      
    /**
     * DbuiFieldType.getAugmentedFieldList
     *  returns a promise -> array of objects describing the fields of bo className
     *  {
     *    fieldName:'field name - dotted if ref sub-field',
     *    fieldLabel:'from i18n getBoLabelGroup, for language of current user',
     *    td: typedesc object copied from bo metadata
     *    refPlaceholder: boolean - true if this is a placeholder for a reference field,
     *    expand: function to expand this placeholder into entries for reference fields
     *  }
     **/
    this.getAugmentedFieldList = function(className, options) {
        
        var Model = db[className];
        var typeDescMap = Model._bo_meta_data.type_desc_map;
        var boLabelGroup = Model._bo_meta_data.field_labels;
        var resultList = [];
        
        options = options || {};
        const denormOnly = options.denormOnly;
        const searchableOnly = options.searchableOnly;
        const includePlaceholders = options.includePlaceholders;


        for(var f in typeDescMap) {
            if(f.indexOf('_') === 0)
                continue;
            
            var td = typeDescMap[f];
            
            //searchableOnly -> don't include fields that don't have queryOps
            if(searchableOnly && !getOpList(td)) 
                continue;
            
            var fieldLabel = boLabelGroup.getAbbreviatedLabel(f, true);
            var fieldInfo = {
                fieldName:f,
                fieldLabel,
                td:td
            };
            resultList.push(fieldInfo);
            
            //reference and composite expandable placeholders work for arrays too:
            var isArray = td instanceof Array;
            if(isArray) {
                td = td[0];
            }
            
            if(td.type === 'reference' && (!denormOnly || td.denormalize_fields)) {
                
                fieldInfo.hasSubfields = true;
                fieldInfo.subTypeDescMap =  db[td.ref_class]._bo_meta_data.type_desc_map;
                
                if(denormOnly && td.denormalize_fields) {
                    fieldInfo.filterSubfields = td.denormalize_fields;
                }
                
                fieldInfo.getSubfieldItems = getSubfieldItems.bind(fieldInfo, boLabelGroup);
                
                //Create placeholder for digging into all fields of a referenced object
                if(includePlaceholders) {
                    var fiPlaceholder = {
                        parent:fieldInfo,
                        fieldName:f+'.',
                        fieldLabel:fieldLabel+'...',
                        td:td,
                        refPlaceholder:true
                    };
                    
                    // fiPlaceholder.expand = expandRefPlaceholder.bind(fiPlaceholder, resultList, boLabelGroup, denormOnly);
                    fiPlaceholder.expand = expandPlaceholder.bind(fiPlaceholder, resultList);
                    resultList.push(fiPlaceholder);
                }
            }
            else if(td.is_composite || td.type === 'composite') {
                
                fieldInfo.hasSubfields = true;
                fieldInfo.subTypeDescMap =  td.type_desc_map;
                fieldInfo.getSubfieldItems = getSubfieldItems.bind(fieldInfo, boLabelGroup);
                
                if(includePlaceholders) {
                    var fiPlaceholder = {
                        parent:fieldInfo,
                        fieldName:f+'.',
                        fieldLabel:fieldLabel+'...',
                        td:td,
                        refPlaceholder:true
                    };
                    
                    // fiPlaceholder.expand = expandCompPlaceholder.bind(fiPlaceholder, resultList, boLabelGroup);
                    fiPlaceholder.expand = expandPlaceholder.bind(fiPlaceholder, resultList);
                    resultList.push(fiPlaceholder);
                }
            }
        
        }//end typeDescMap iteration
        
        resultList.sort(function(a, b) {
            if (a.fieldLabel < b.fieldLabel)
                return -1;
            if (a.fieldLabel > b.fieldLabel)
                return 1;
            return 0;
        });

        
        return resultList;
    };
    

}