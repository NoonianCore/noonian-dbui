function ($stateParams, $scope, DbuiAction, NoonI18n, theObject, viewPerspective,DbuiPerspectiveEditor,$timeout) {

    
    var className = $scope.boClass;
    
    if(!className) {
        className = $scope.boClass = $stateParams.className;
    }
    
    $scope.theObject = theObject;
    $scope.viewPerspective = viewPerspective;


    $scope.boId = $stateParams.id;
    
    
    if(!theObject) {
        return;
    }

    $scope.labels = NoonI18n.getBoLabelGroup(className);
    
    
    // function(perspectiveObj, contextBo, actionObj, argsObj)
    $scope.invokeAction = DbuiAction.invokeAction.bind(DbuiAction, viewPerspective, theObject);
    
    if(viewPerspective.actions) {
        $scope.actionList = DbuiAction.unaliasActionList(viewPerspective.actions);
    }
    
    if(viewPerspective.recordActions) {
        $scope.recordActionList = DbuiAction.unaliasActionList(viewPerspective.recordActions);
        DbuiAction.processActionVisibility($scope.recordActionList, theObject);
    }
    
        
    if(viewPerspective.allowEditLayout) {
        $scope.editLayout = function() {
            DbuiPerspectiveEditor.showLayoutEditorDialog(className, viewPerspective, 'view').then(result=>{
                
                if(result) {
                    viewPerspective.layout = result;
                    //Refresh the object editor
                    $scope.refreshing = true;
                    $timeout(function() {
                        $scope.refreshing = null;
                    }, 100);
                }
            });
        };
    }
    
    if($scope.setPageTitle)
        $scope.setPageTitle('View '+theObject._disp);

}