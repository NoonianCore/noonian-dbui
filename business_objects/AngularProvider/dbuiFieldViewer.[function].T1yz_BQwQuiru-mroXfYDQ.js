function ($compile,  DbuiFieldType, $injector) {
    return {
      template: '<div></div>',
      restrict: 'E',
      
      scope: {
        displayValue: '=',
        typeDesc: '<',
        abbreviated: '<?',
        fieldCustomizations: '<?', //From perspective.field_customizations
        contextObject: '<?'
      },
      
      link: function (scope, element, attrs, ngModel) {
          var td = scope.typeDesc;
          if(!td) {
              console.error('Missing typeDesc in dbuiFieldEditor directive');
              return;
          }
          
          DbuiFieldType.getSpec(td, 'view', scope.fieldCustomizations).then(function(specObj){
              
                if(!specObj) {
                    console.error('Missing UI Spec in dbuiFieldViewer directive:', td, scope.fieldCustomizations);
                    return;
                }
                
              var templateHtml = specObj.template;
              var compiledTemplate = $compile(templateHtml);
              
              element.append(compiledTemplate(scope));
              
              if(specObj.link_fn) {
                  var fnString = specObj.link_fn;
                  try {
                      var toCall;
                      eval("toCall = "+fnString);
                      if(typeof toCall === 'function') {
                          $injector.invoke(toCall, this, 
                            {scope:scope, iElement:element, iAttributes:attrs}
                            );
                      }
                      else {
                          console.error('bad link function for typeDesc', td);
                      }
                  }
                  catch(err) {
                      console.error('bad link function for typeDesc', td, err);
                  }
                  
                }
              
          });

      },
       
      controller: function($scope) {
          DbuiFieldType.getSpec($scope.typeDesc, 'view', $scope.fieldCustomizations).then(function(specObj){
              
              if(specObj && specObj.controller_fn) {
                  var fnString = specObj.controller_fn;
                  try {
                      var toCall;
                      eval("toCall = "+fnString);
                      if(typeof toCall === 'function') {
                          return $injector.invoke(toCall, this, {$scope:$scope});
                      }
                      else {
                          console.error('bad controller function for typeDesc', $scope.typeDesc);
                      }
                  }
                  catch(err) {
                      console.error('bad controller function for typeDesc', $scope.typeDesc, err);
                  }
              }
          
          });
      }
      
    };
  }