function (Dbui, NoonI18n,DbuiAction) {
  return {
      
    templateUrl: function(elem, attr) {
        if(attr.expanded !== 'true') {
            return 'dbui2/reusable/core/object_editor.html';
        }
        else {
            return 'dbui2/reusable/core/object_editor_expanded.html';
        }
    },
    
    restrict: 'E',
    scope: {
      theObject: '=',  //Object being displayed (a model instance from datsource)
      perspective: '=',
      formStatus: '=?',
      expanded:'@?'
    },
    
    link:function(scope) {
        
        //When updates come from form/ngModel -> reflect in optional formStatus object
        //  resetting dirty status needs to be done externally;
        //  i.e. after a save, formStatus.isDirty should be set to false
        scope.$watch('editorForm.$dirty', function(isDirty) {
            console.debug('dbuiObjectEditor.watch(editorForm.$dirty)', isDirty);
            if(scope.formStatus && isDirty) {
                scope.formStatus.isDirty = true;
            }
        });
        
        //Form dirty status can be reset by updating the optional formStatus object
        scope.$watch('formStatus.isDirty', function(isDirty) {
            console.debug('dbuiObjectEditor.watch(formstatus.isDirty)', isDirty);
            if(!isDirty) {
                scope.editorForm.$setPristine();
            }
        });
        
        const resetDirtyStatus = function(evt, specObj) {
            console.debug('dbuiObjectEditor.resetDirtyStatus', specObj.key);
            scope.editorForm.$setPristine();
            if(scope.formStatus) scope.formStatus.isDirty = false;
            
            //No need to bubble up to parent scope
            evt && evt.stopPropagation && evt.stopPropagation();
            
        };
        
        //the dbui-field-editor emits an event when it's link function is complete.
        // use that to initialize "pristine/dirty" status of the form, since
        // some of the more complicated asyncronous initilization prematurely 
        // dirties the form.
        scope.$on(Dbui.EVENTS.fieldInitComplete, resetDirtyStatus);
    },
    
    controller:function($scope, $parse) {

      var theObject = $scope.theObject;
      var perspective = $scope.perspective;

      var className = theObject._bo_meta_data.class_name;
      
      $scope.labelGroup = theObject._bo_meta_data.field_labels;
      
      $scope.typeDescMap = theObject._bo_meta_data.type_desc_map;


      $scope.colClass = Dbui.columnClasses;

      //Set up getter/setter function with ngModel;
      // allows us to have dotted subfields in the layout,
      // resulting in editable subfields
      var getterSetterFn = function(fieldName, value) {

        if(arguments.length > 1) {
          //called as setter
          _.set(theObject, fieldName, value);
        }

        return _.get(theObject, fieldName);

      };

      var getterSetter = $scope.getterSetter = {};

      //Traverse the normalized layout: section -> rows -> field names
      _.forEach(perspective.layout, function(section) {
        _.forEach(section.rows, function(row) {
          for(var i=0; i < row.length; i++) {
            var f = row[i];
            getterSetter[f] = getterSetterFn.bind(null, f);
          }
        });
      });


      var fieldCustomizations = perspective.fieldCustomizations || {};
      var displayCheckers = {};
      var contextFields = {}; //maps dotted field names present in fieldCustomizations to the base field name
      
      
      var requiredFields = {};
      var reqExpression = '';
      var reqChecker;
      var reqWatcher = function() {
          $scope.formStatus.isValid = !!reqChecker($scope.theObject);
      };

      for(var f in fieldCustomizations) {
        if(fieldCustomizations[f].conditionalDisplay) {
          displayCheckers[f] = $parse(fieldCustomizations[f].conditionalDisplay);
          var dotPos = f.indexOf('.');
          if(dotPos > -1) {
              contextFields[f] = f.substring(0, dotPos);
          }
        }
        
        if(fieldCustomizations[f].required) {
            requiredFields[f] = true;
            reqExpression += (reqExpression ? ' && ' : '')+f;
            $scope.$watch('theObject.'+f, reqWatcher);
        }
        
        if(fieldCustomizations[f].fieldActions) {
            var fa = $scope.fieldActions = $scope.fieldActions || {};
            fa[f] = DbuiAction.unaliasActionList(fieldCustomizations[f].fieldActions);
        }
      }
      
      if(reqExpression) {
          reqChecker = $parse(reqExpression);
      }
      
      $scope.isRequired = function(field) {
          if(!requiredFields[field]) {
              return false;
          }
          else {
              return !_.get($scope.theObject, field);
          }
      };

      $scope.shouldShow  = function(field) {
          if(!$scope.getTypeDesc(field)) {
              return false;
          }
          var dc = displayCheckers[field];
          if(!dc) {
              return true;
          }
          else {
              var context = contextFields[field] ? _.get($scope.theObject, contextFields[field]) : $scope.theObject;
              return dc(context);
          }
      };
      
      var tdCache = {};
      $scope.getTypeDesc = function(field) {
          if(!tdCache[field]) {
              tdCache[field] = $scope.typeDescMap.getTypeDescriptor(field);
          }
          
          return tdCache[field];
      };
      
      $scope.invokeFieldAction = function(action, fieldName) {
          return DbuiAction.invokeAction($scope.perspective, $scope.theObject, action, {fieldName, fieldValue:$scope.theObject[fieldName]});
      };

    }
  };
}