function ($http, $q, db, $state, $injector, $window, NoonWebService, $rootScope) {
    const THIS_SVC = this;
    
    var errObj = function(key, err) {
      return {error:'Error invoking action '+key+' - '+err};
    };
    var uiActionCache = {};

    const invokeUiAction = function(actionObj, params, thisArg) {
      var key = actionObj.ui_action;
      var deferred = $q.defer();

      var queryPromise;

      if(uiActionCache[key]) {
        queryPromise = $q.when(uiActionCache[key]);
      }
      else {

        queryPromise = NoonWebService.call('sys/getUiAction', {key:key}).then(
          function(actionObj) {
            
            if(!actionObj) {
              return deferred.resolve({error:'No UiAction with key '+key});
            }

            var fnString = actionObj.function;
            try {
              var toCall;
              eval("toCall = "+fnString);
              if(typeof toCall === 'function') {
                uiActionCache[key] = toCall;
                return toCall;
              }
              else {
                deferred.resolve(errObj(key, "function parse failed"));
              }
            }
            catch(err) {
              deferred.resolve(errObj(key, err));
            }
          },
          function(err) {
            deferred.resolve(errObj(key, err));
          }
        );
      }

      queryPromise.then(
        function(actionFn) {
          if(typeof actionFn === 'function') {
            try {
              var callResult = $injector.invoke(actionFn, params);
              deferred.resolve(callResult);
              // deferred.resolve(actionFn.call(thisArg, parameters));
            }
            catch(err) {
              deferred.resolve(errObj(key, err));
            }
          }
        });

      return deferred.promise;
    };

    const invokeWsCall = function(actionObj, params) {
        
      var parameters = {};
      _.assign(parameters, actionObj.params || {}, params || {});

      if(!actionObj.origIcon) {
        actionObj.origIcon = actionObj.icon;
      }
      actionObj.icon = 'fa-spinner fa-spin';
      return NoonWebService.call(actionObj.ws, parameters).finally(()=>{
          actionObj.icon = actionObj.origIcon;
      });
    };
    
    const invokeBroadcast = function(actionObj, params) {
        var parameters = {};
        _.assign(parameters, actionObj.params || {}, params || {});
        return $rootScope.$broadcast(actionObj.broadcast, params);
    };
    
    
    const invokeSequence = function(actionObj, params, thisArg) {
        var promChain = $q.resolve(true);
        _.forEach(actionObj.sequence, function(a) {
            promChain = promChain.then(THIS_SVC.invoke.bind(THIS_SVC, a, params, thisArg));
        });
        return promChain;
    };
    
    const invokeMemberFn = function(actionObj, params) {
     
      const targetObj = params.targetObj;
      const fnName = actionObj.member_function;
      
      const parameters = {};
      _.assign(parameters, actionObj.params || {}, params || {});
      
      
      if(targetObj[fnName] instanceof Function) {
          //client-side member function - invoke w/ $injector
          return $q.when($injector.invoke(targetObj[fnName], targetObj, parameters));
      }
      
      //Server-side member function
      parameters._target_id = targetObj._id;
      parameters._target_fn = fnName;
      delete parameters.perspectiveObj;
      delete parameters.targetObj;
      
      if(!actionObj.origIcon) {
        actionObj.origIcon = actionObj.icon;
      }
      actionObj.icon = 'fa-spinner fa-spin';
      return NoonWebService.call('dbui2/invokeMemberFunction', parameters).finally(result=>{
        actionObj.icon = actionObj.origIcon;
      });
    };
    

    var aliases = {};

    this.invoke = function(actionObj, actionArgs, thisArg) {
    //   console.log('NoonAction.invoke', actionObj, actionArgs, thisArg);
      if(typeof actionObj === 'string') {
        actionObj = aliases[actionObj];
      }

      if(!actionObj) {
        var err = 'Invalid actionObj parameter';
        console.error(err);
        return $q.reject(err);
      }
      
      if(actionObj.confirm && !confirm(actionObj.confirm)) {
          return {result:'cancelled'};
      }

      var passedArgs = {};
      _.assign(passedArgs, actionObj.params||{}, actionArgs||{});

      if(actionObj.state) {
          if(!actionObj.launch_new_window) {
            return $state.go(actionObj.state, passedArgs);
            
          }
          else {
              //load state in new window.
              // Only works for bookmark-able states!
              var targetUrl = $state.href(actionObj.state, passedArgs, {absolute:true});
              $window.open(targetUrl);
              return $q.resolve(true);
          }
      }
      else if(actionObj.ui_action) {
        return invokeUiAction(actionObj, passedArgs, thisArg);
      }
      else if(actionObj.ws) {
        return invokeWsCall(actionObj, passedArgs);
      }
      else if(actionObj.fn) {
        return $q.resolve(actionObj.fn.apply(thisArg || actionObj, [passedArgs]));
      }
      else if(actionObj.broadcast) {
        return $q.resolve(invokeBroadcast(actionObj, passedArgs));
      }
      else if(actionObj.sequence) {
        return invokeSequence(actionObj, passedArgs, thisArg);
      }
      else if(actionObj.return) {
        return $q.resolve(actionObj.return);
      }
      else if(actionObj.external) {
        //TODO allow for parameter substituion via passedArgs
        return $window.open(actionObj.external);
      }
      else if(actionObj.member_function) {
        return invokeMemberFn(actionObj, passedArgs, thisArg);
      }
      else {
        err = 'Attempted to invoke invalid action';
        console.error(err, actionObj);
        return $q.reject(err);
      }
    };
    
    this.registerAlias = function(alias, actionObj) {
        
        if(alias && actionObj) {
          aliases[alias] = _.clone(actionObj);
        }
    };
    
    this.registerAliases = function(aliasMap) {
        _.forEach(aliasMap, function(actionObj, alias) {
            aliases[alias] = _.clone(actionObj);
        });
    };
    
    this.unalias = function(aliasString) {
        return aliases[aliasString];  
    };
    
    this.unaliasActionList = function(actionList, otherAliases) {
        otherAliases = otherAliases || {};
        
        var result = [];
        _.forEach(actionList, function(a) {
            if(angular.isString(a)) {
                var resolved = otherAliases[a] || aliases[a];
                if(resolved) {
                    result.push(resolved);
                }
            }
            else {
                result.push(a);
            }
        })
        
        return result;
    };


  }