function ($scope, $state, $stateParams, $timeout, $q, $filter, $rootScope, $window, db, Dbui, DbuiFieldType, DbuiAlert, DbuiAction) {
    
    const roleId = $state.params.id;
    
    
    const perspectiveRegex = '^sys.dbui.(perspective|displayoptions)';
    
    const rolespecs = [
        
        {
            title:'Menus',
            class:'Menu',
            field:'rolespec'
        },
        {
            title:'Perspectives',
            class:'Config',
            field:'rolespec',
            extraFilter:{key:{$regex:perspectiveRegex}}
        },
        {
            title:'Custom Pages',
            class:'DbuiCustomPage',
            field:'rolespec'
        },
        {
            title:'DACs',
            class:'DataAccessControl',
            field:'rolespec'
        },
        {
            title:'Web Services',
            class:'WebService',
            field:'rolespec'
        },
        {
            title:'Custom Config',
            class:'Config',
            field:'rolespec',
            extraFilter:{key:{$not:{$regex:perspectiveRegex}}}
        }
        
    ];
    
    const access = [
        'read',
        'create',
        'update',
        'delete'
    ];
    
    $scope.accessCss = {
        'true':'success',
        'c':'warning',
        undefined:'danger'
    }
    
    const summaryDesc = obj=>{
        var full = true;
        var ret = [];
        access.forEach(a=>{
            let val = obj[a];
            full = full && obj[a];
            if(val === 'c') {
                ret.push(a+'(cond)');
            }
            else if(val) {
                ret.push(a);
            }
        });
        
      
      if(full) return 'FULL';
      
      return ret.join(',');
    };
    
    const getDacSummary = function(role) {
        return db.DataAccessControl.find({rolespec:role._id}).$promise.then(dacList=>{
            
            
            const summaryMap = {}; //map BOD Class to {CRUD}
            
            _.forEach(dacList, dac=>{
                const classes = _.pluck(dac.business_object, '_disp');
                const cond = dac.condition ? 'c' : true;
                
                classes.forEach(className=>{
                    var m = summaryMap[className] = summaryMap[className] || {className, dacs:{}};
                    access.forEach(a=>{
                        if(dac['allow_'+a]) {
                            m[a] = cond;
                            m.dacs[a] = dac;
                        }
                    })
                });
                
            });//end dacList iteration
            
            const summaryList = $scope.dacSummary = [];
            Object.keys(summaryMap).sort().forEach(className=>{
                let s = summaryMap[className];
                s.summary = summaryDesc(s);
                summaryList.push(s);
            });
        });
    };
    
    $scope.showDac = function(dac) {
        if(dac)
            $state.go('dbui.edit', {id:dac._id, className:'DataAccessControl', perspective:'default'});
    };
    
    db.Role.findOne({$or:[{_id:roleId}]}).$promise.then(r=>{
        
        getDacSummary(r);
        
        $scope.theRole = r;
        const tabs = $scope.tabs = [];
        
        const perspectivePromises = [];
        rolespecs.forEach(rs=>{
            perspectivePromises.push(
                Dbui.getPerspective('default', rs.class, 'list')
            );
        });
        
        $q.all(perspectivePromises).then(function(perspectiveArr) {
            var i = 0;
            var countPromises = [];
            
            rolespecs.forEach(rs=>{
                var perspective = _.clone(perspectiveArr[i++]);
                
                var f = perspective.filter = {$comment:'applies to '+r.name};
                
                f[rs.field] = r._id;
                
                if(rs.extraFilter) {
                    _.assign(f, rs.extraFilter);
                }
                
                countPromises.push(db[rs.class].find(f, {_id:1}).$promise);
                
                
                tabs.push({
                    title:rs.title,
                    class:rs.class,
                    perspective
                });
                
            }); //end rolespec iteration
            
            
            return $q.all(countPromises).then(counts=>{
                var i=0;
                counts.forEach(c=>{
                    tabs[i++].count = c.length;
                })
            })
            
        })
    })
}