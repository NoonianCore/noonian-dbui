function (db, $scope, NoonWebService, $stateParams, $q, DbuiAlert, DbuiAction, $state, $rootScope, $uibModal) {
    
    $scope.waiting = 'Getting list of updates...';
    var fullList;
    var buildConfig;
    var bop;
    const otherPackages = $scope.otherPackages = [];
    
    NoonWebService.call('pkg/dataForGenerate', {id:$stateParams.id}).then(result=>{
        console.log('dataForGenerate', result);
        fullList = result.updates;
        buildConfig = result.buildConfig;
        bop = $scope.bop = result.bop;
        
        Object.keys(buildConfig).forEach(k=>{
            if(k !== '_enabled' && k !== bop.key) {
                otherPackages.push(k);
            }
        });
        
        
        
        
        _.forEach(fullList, r=>{
            r.include = true;
        });
        
        var grouped = _.groupBy(fullList, 'boClass');
        
        var om = $scope.objectMap = {};
        
        _.forEach(grouped, (list, boClass)=>{
            om[boClass] = _.sortBy(list, 'disp');
        });
        
        
        
        
        $scope.classes = Object.keys($scope.objectMap).sort();
        
        $scope.waiting = null;
    });
    
    
    const showVersionDialog = function() {
        $scope.waiting = 'Get Version';
        
        var modalScope = $rootScope.$new(true);
        modalScope.pkg = $scope.bop;
        modalScope.params = {
            version:modalScope.pkg.version,
            inc:'patch',
            prerelease:true,
            meta:''
        };
        
        modalScope.$watch('params', p=>{
            // console.log('params', p);
            NoonWebService.call('/sys/util/semver', p).then(result=>{
                modalScope.genVersion = result;
            });
        }, true);
        
        
        var modal = $uibModal.open({
            templateUrl:'sys/pkg_mgmt/dialog/generate.html',
            scope:modalScope,
            size:'sm'
        });
        
        
        modalScope.generate = ()=>modal.close(modalScope.genVersion);
        
        return modal.result;
    };
    
    
    const reassignExcludes = function(excludeList) {
        var modalScope = $rootScope.$new(true);
        modalScope.pkg = $scope.bop;
        modalScope.excludeList = excludeList;
        modalScope.otherPackages = otherPackages;
        
        
        var modal = $uibModal.open({
            templateUrl:'sys/pkg_mgmt/dialog/reassign_ul.html',
            scope:modalScope,
            size:'lg'
        });
        
        return modal.result;
    };
    
    const processIncludeExclude = function() {
         var excludeList = [];
        _.forEach(fullList, obj=>{
            if(!obj.include) {
                // excludeList = excludeList.concat(_.pluck(obj.updateLog, '_id'));
                obj.targetPkg = null;
                excludeList.push(obj);
            }
        });
        
        $scope.waiting = 'Process Include/Exclude';
        
        var prom = $q.when(null);
        if(excludeList.length) {
            
            //if there are other packages being built on this instance, 
            // allow the user to re-assign excluded updates to one of them
            if(otherPackages.length) {
                prom = reassignExcludes(excludeList);
            }
            
            prom = prom.then(()=>{
                const reassignMap = {};
                _.forEach(excludeList, obj=>{
                    reassignMap[obj.id] = obj.targetPkg;
                });
                console.log('reassignMap', reassignMap);
                return NoonWebService.call('pkg/deactivateUpdateLogs', {reassign:reassignMap})
            });
        }
        
        return prom;
    };
    
    $scope.generate = function() {
        
        return processIncludeExclude()
            .then(showVersionDialog)
            .then(version=>{
                console.log('Generating version', version);
                $scope.waiting = 'Generating package';
                
                
                return NoonWebService.call('pkg/buildPackage', {id:$stateParams.id,version}).then(r=>{
                    console.log(r);
                    DbuiAlert.success(r.message);
                    $state.go('dbui.view', {className:'BusinessObjectPackage', perspective:'default', id:$stateParams.id});
                },
                err=>{
                    console.error(err);
                    DbuiAlert.danger('Error generating package');
                    $scope.waiting = null;
                });
        
        
            },
            err=>{
                //Probably just clicked "cancel" on dialog
                $scope.waiting = null;
            });
        //
    };
    
    $scope.showRecord = function(className, id) {
        var actionObj = DbuiAction.unalias('dialog-view');
        let params = {
            id,
            className
        };
        DbuiAction.invokeAction('default', {_id:id}, actionObj, params);
    };
      
}