function($scope, $rootScope, $state, NoonWebService, $uibModal, DbuiAlert, $timeout) {
    $scope.initialized = false;
    
    $scope.activeTab = 'current';
    
    const init = function() {
        NoonWebService.call('pkg/localInstanceStatus').then(s=>{
            console.log('localStatus', s);
            $scope.localStatus = s;
            $scope.initialized = true;
        });
    };
    init();
    $scope.$on('dbui.pkg_mgmt.refresh_current', init); //emitted after package is installed
    
    var deregisterRefreshWatch = $rootScope.$on('dbui.refresh', function(evt, params) {
        if(params && params.className === 'BusinessObjectPackage') {
            $scope.initialized = false;
            $timeout(init);
        }
    });
    var deRegisterStateChange = $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
        deregisterRefreshWatch();
        deRegisterStateChange();
    });
    
    
    
    
    $scope.edit = function(pkg) {
        $state.go('dbui.edit', {
            perspective:'default',
            className:'BusinessObjectPackage', 
            id:pkg._id
        });
    };
    
    $scope.manifest = function(pkg) {
        
        var modalScope = $rootScope.$new(true);
        modalScope.pkg = pkg;
        
        
        var modal = $uibModal.open({
            templateUrl:'sys/pkg_mgmt/dialog/manifest.html',
            scope:modalScope,
            size:'lg'
        });
        
        
        NoonWebService.call('pkg/getAugmentedManifest', {key:pkg.key}).then(manifestMap=>{
            modalScope.objectMap = manifestMap;
            modalScope.classes = Object.keys(manifestMap).sort();
        },
        err=>{
            console.error(err);
            modal.close();
        });
    };
    
    $scope.upgrade = function(pkg) {
        //Hand this off to the "install" tab 
        $scope.activeTab = 'install';
        $scope.$broadcast('dbui.pkg_mgmt.upgrade', pkg);
    };
    
    $scope.uninstall = function(pkg) {
        console.log('Uninstall', pkg);
        NoonWebService.call('pkg/uninstallPackage', {id:pkg._id}).then(precheck=>{
            console.log('uninstall pre-check', precheck);
            var modalScope = $rootScope.$new(true);
            modalScope.pkg = pkg;
            modalScope.precheck = precheck;
            
            modalScope.modifiedCount = 0;
            let modList = modalScope.modifedList = [];
            
            modalScope.otherCount = 0;
            let othList = modalScope.otherList = [];
            
            _.forEach(precheck.modified, (idList, className)=>{
                modList.push({className, idList});
                modalScope.modifiedCount += idList.length;
            });
            
            _.forEach(precheck.other_objects, (idList, className)=>{
                othList.push({className, idList});
                modalScope.otherCount += idList.length;
            });
            
            var modal = $uibModal.open({
                templateUrl:'sys/pkg_mgmt/dialog/confirm_uninstall.html',
                scope:modalScope,
                size:'lg'
            });
            
            modal.result.then(()=>{
                console.log('PERFORMING UNINSTALL', pkg);
                let wsParams = {
                    id:pkg._id,
                    confirmed:true,
                    keep_data:precheck.keepData
                };
                NoonWebService.call('pkg/uninstallPackage', wsParams).then(r=>{
                     console.log(r);
                     DbuiAlert.success('Uninstall Successful');
                     $rootScope.$broadcast('dbui.refresh', {className:'BusinessObjectPackage'});
                },
                err=>{
                    console.error(err);
                    DbuiAlert.danger('Uninstall failed - check log');
                });
            },
            err=>{
                console.log('cancelled uninstall');
            });
        });
    };
    
    $scope.build = function(pkg) {
        $state.go('dbui.sys_build_pkg', { 
            id:pkg._id
        });
    };
    
    $scope.refRepair = function() {
        $scope.waiting = true;
        NoonWebService.call('sys/util/ref_repair').then(r=>{
            DbuiAlert.success('Successful result for index repair/rebuild');
            $scope.waiting = false;
        },
        err=>{
            DbuiAlert.error(err);
            $scope.waiting = false;
        });
    };
    
}