function ($scope, $state, $stateParams, $timeout, $q, $filter, $rootScope, $window, db, Dbui, DbuiFieldType, DbuiAlert, DbuiAction, NoonWebService) {
    
    console.log('dbui.pivot_page', $stateParams);
    
    const params = $state.params || {};
    
    const perspectiveName = $scope.perspectiveName = params.perspective || 'default';
    const launchClass = params.className;
    const launchId = params.id;
    const launchInitObj = params.initObj;
    
    
    
    if(!launchClass || !(launchId || launchInitObj)) {
        DbuiAlert.danger('Missing required parameters');
        return;
    }
    
    console.log('initializing pivot page', params);
    
    var centralObj, centralClass, launchObj, launchObjPerspective;
    var pageConfig;
    
    var tabs, tabIndex;
    
    
    ///////////////////////////
    //TAB MGMT ////////////////
    ///////////////////////////
    
    var tabStatus = $scope.tabStatus = {history:[]};
    
    

    const showTab = $scope.showTab = function(tabId) {
        $timeout(function() {
            // console.log('switching tab to: '+tabId);
            $scope.tabStatus.activeTab = tabId;
        },100);
    };
    
    const newTab = $scope.newTab = function(tabSpec, afterTab) {
        if(tabIndex[tabSpec && tabSpec.tabId]) {
            return showTab(tabSpec.tabId);
        }
        var insertPos = (tabs.indexOf(afterTab)+1) || tabs.length;
        tabs.splice(insertPos, 0, tabSpec);
        tabIndex[tabSpec.tabId] = tabSpec;
        showTab(tabSpec.tabId);
    };
    
    
    const closeTab = $scope.closeTab = function(tabSpec) {
        
        if(tabSpec.formStatus && tabSpec.formStatus.isDirty && 
            !confirm('WARNING: you have unsaved changes.  Are you sure you want to close without saving?')) {
            return;
        }
        
        var delPos = tabs.indexOf(tabSpec);
        if(delPos > -1) {
            tabs.splice(delPos, 1);
            delete tabIndex[tabSpec.tabId];
            if(tabStatus.prevTab) {
                showTab(tabStatus.prevTab);
            }
            
            if(tabSpec.isExtra) {
                $scope.extraTabs.push(tabSpec);
            }
        }
        else {
            console.error('Attempted closing invalid tab:', tabSpec);
        }
    };
    
    $scope.showExtraTab = function(tabSpec) {
        const et = $scope.extraTabs;
        let pos = et.indexOf(tabSpec);
        et.splice(pos, 1);
        newTab(tabSpec);
    }
    
    
    //invoked when tab is clicked
    $scope.onTabChanged = function(targetTab) {
        if(targetTab.tabId != tabStatus.activeTab) {
            tabStatus.prevTab = tabStatus.activeTab;
        }
        // console.log('tab change', targetTab.tabId);
        // (this works because uib-tab's select is evaluated before uib-tabset's active is updated!)
    };
    
    
    $scope.deleteCentralObj = function() {
        if(confirm(`Are you sure you want to delete ${centralObj._disp}?`)) {
            centralObj.remove().then(function() {
                $window.history.back();
            });
        }
    }
    
    const initTabs = function(tabList) {
        var needKeys = {};
        var tabParams = {};
        _.forEach(tabList, (t, i)=>{
            if(typeof t === 'string') {
                needKeys[t] = i;
            }
            else if(t.key) {
                needKeys[t.key] = i;
                tabParams[t.key] = t.params;
            }
        });
        
        var keyArr = Object.keys(needKeys);
        if(!keyArr.length) {
            // return $q.when(tabList);
            return tabList;
        }
        
        return NoonWebService.call('dbui/getTabDefs', {keys:keyArr, centralClass}).then(tdList=>{
            var ret = angular.copy(tabList);
            
            var extra = $scope.extraTabs = [];
            
            _.forEach(tdList, tabdef=>{
                var pos = needKeys[tabdef.key];
                if(pos > -1) {
                    ret[pos] = tabdef;
                    let tp = tabParams[tabdef.key];
                    tp && _.assign(tabdef, tp);
                }
                else {
                    tabdef.isExtra = true;
                    extra.push(tabdef);
                }
            });
            console.log('extra tabs', extra);
            return ret;
        });
        
        
    };
    
    const launchObjPromise = 
        launchId ? 
            db[launchClass].findOne({_id:launchId}).$promise :
            $q.when(launchInitObj);
    
    //Grab launch object and its perspective
    $scope.initPromise = 
    $q.all([
        launchObjPromise,
        Dbui.getPerspective(perspectiveName, launchClass, 'pivot_page')
    ])
    .then(resultArr=>{
        launchObj = $scope.launchObj = resultArr[0];
        launchObjPerspective = resultArr[1];
        
        if(!params.referenceField) {
            centralClass = $scope.centralClass = launchClass;
            return [launchObj, launchObjPerspective, initTabs(launchObjPerspective.tabs)];
        }
        else {
            //Dig in to the reference to find the central object...
            let centralRef = launchObj[params.referenceField];
            // console.log('centralRef', centralRef);
            let refTd = db[launchClass]._bo_meta_data.type_desc_map.getTypeDescriptor(params.referenceField);
            centralClass = $scope.centralClass = refTd.ref_class;
            
            if(!centralRef || !centralRef._id) {
                let errMsg = `Reference to ${centralClass} not found for this ${launchClass} in field ${params.referenceField}`;
                DbuiAlert.danger(errMsg);
                throw errMsg;
            }
            
            
            return Dbui.getPerspective(perspectiveName, centralClass, 'pivot_page').then(centralPerspective=>{
                
                //Merge in the launch object's perspective into the main central object's
                let mergedPerspective = centralPerspective;
                if(launchObjPerspective && launchObjPerspective[params.referenceField]) {
                    launchObjPerspective = launchObjPerspective[params.referenceField];
                    
                    let allTabs = (centralPerspective.tabs || []).concat(launchObjPerspective.tabs || []);
                    mergedPerspective = angular.copy(centralPerspective);
                    _.assign(mergedPerspective, launchObjPerspective);
                    mergedPerspective.tabs = allTabs;
                }
                
                return $q.all([
                    db[centralClass].findOne({_id:centralRef._id}).$promise,
                    mergedPerspective,
                    initTabs(mergedPerspective.tabs)
                ]);
            });
        }
    },
    err=>{ console.error(err); })
    .then(resultArr=>{
        centralObj = $scope.centralObj = resultArr[0];
        pageConfig = $scope.pageConfig = angular.copy(resultArr[1]);
        tabs = resultArr[2];
        tabIndex = _.indexBy(tabs, 'tabId');
        
        if(!pageConfig) {
            console.error('MISSING PORTAL CONFIG FOR PERSPECTIVE '+perspectiveName);
            return;
        }
        
        
        if(pageConfig.titleExp) {
            pageConfig.title = $scope.$eval(pageConfig.titleExp, centralObj)
        }
        
        if(pageConfig.initAction) {
            DbuiAction.invoke(pageConfig.initAction, {portalScope:$scope});
        }
        
        _.forEach(tabs, t=>{
            if(t.hideCondition) {
                $scope.$watch(t.hideCondition, val=>{
                    t.isHidden = val;
                })
            }
        })
        pageConfig.tabs = tabs;
        
        return centralObj;
    });
    
    
    
    var deRegister = $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
        var warn = false;
        _.forEach(tabs, function(t) {
            if(t.waiting) {
                warn = t.waitingMessage || 'An operation is in progress.  Abort?';
            }
        });
        
        if(!warn) {
            _.forEach(tabs, function(t) {
                if(t.formStatus && t.formStatus.isDirty) {
                    warn = 'WARNING: you have unsaved changes.  Are you sure you want to navigate away without saving?';
                }   
            });
        }
        
        if(warn && !confirm(warn)){
            event.preventDefault();
        }
        else {
            deRegister();
        }
    });
    
    
    
    $scope.msgClicked = function(msg) {
        $scope.$broadcast(msg.action, msg);
    };
    
    $scope.invokeAction = function(a) {
        // DbuiAction.invokeAction(perspectiveObj, contextBo, actionObj, argsObj);
        DbuiAction.invokeAction(null, $scope.centralObj, a, {className:centralClass});
    }
    
    //Utility so tabs can have direct access to the top-level scope.
    $scope.getSupportPortalScope = function() {
        return $scope;
    };
    
    //Utility to allow intercommunication between tabs
    $scope.broadcastToTabs = function(eventName, params) {
        $scope.$broadcast(eventName, params)
    };
    
     $scope.refreshCentralObj = function() {
        const centralId = $scope.centralObj._id; 
        $scope.centralObj = null;
        db[centralClass].findOne({_id:centralId}).$promise.then(function(co) {
            $scope.centralObj = co;
        });
    };
    
}