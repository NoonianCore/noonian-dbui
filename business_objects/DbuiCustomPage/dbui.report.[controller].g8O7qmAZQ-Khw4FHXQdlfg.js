function (db, Dbui, NoonI18n, NoonWebService, BusinessObjectModelFactory, $stateParams, DbuiAction) {
    
    var inputModel;
    var outputModel;
    
    
    
    NoonWebService.call('dbui/getReportSpec', {id:$stateParams.id}).then(function(result) {
        
        var reportSpec = $scope.reportSpec = result.reportSpec;
        var labels = $scope.reportLabels = result.labels;
        
        // db.makeCompositeBo(reportSpec.name+'#input', reportSpec.input_typedef, {});
        inputModel = BusinessObjectModelFactory.getConstructor(
              reportSpec.input_typedef, 
              reportSpec.name+'#input', 
              labels._input_fields
        );
        
        var initialInput = $stateParams.initialInput || {};
        
        $scope.inputObj = new inputModel(initialInput, {reportSpec});
        
        var inputPerspective = $scope.inputPerspective = reportSpec.input_perspective;
        $scope.inputPerspective.layout = Dbui.normalizeLayout($scope.inputPerspective.layout);
        
        
        if(inputPerspective.onLoadAction) {
            DbuiAction.invokeAction(inputPerspective, $scope.inputObj, inputPerspective.onLoadAction, {report:reportSpec})
            .then(function(r) {
                if(r) {
                    $scope.submit();
                }
            });
        }
        
        outputModel = BusinessObjectModelFactory.getConstructor(
              reportSpec.output_typedef, 
              reportSpec.name+'#output', 
              labels._output_fields
        );
        
        var outputPerspective = $scope.outputPerspective = reportSpec.output_perspective;
        outputPerspective.layout = Dbui.normalizeLayout(outputPerspective.layout);
        
        
        
        // can use unaliasActionList if/when we determine if there are any generic/reusable actions 
        //   (e.g. perhaps for showing backing data or downloading the report, saving report?)
        // $scope.resultActions = DbuiAction.unaliasActionList(editPerspective.recordActions, specialActions);
        $scope.resultActions = outputPerspective.actions;
    });
    
    
    $scope.submit = function() {
        $scope.waiting = true;
        $scope.resultObj = null;
        
        NoonWebService.call($scope.reportSpec.web_service.path, {input:$scope.inputObj}).then(function(resp) {
            var resultObj = $scope.resultObj = new outputModel(resp, {
                reportSpec:$scope.reportSpec,
                inputObj:$scope.inputObj
            }); 
            $scope.invokeAction = DbuiAction.invokeAction.bind(DbuiAction, $scope.outputPerspective, resultObj);
            DbuiAction.processActionVisibility($scope.resultActions, resultObj);
            console.log(resultObj);
            $scope.waiting = false;
        },
        function(err) {
            console.log(err);
            $scope.waiting = false;
        });
    };
    
}