function($rootScope) {
    var layoutStatus = $rootScope.layoutStatus = {
        sidebarMinimized:false,
        sidebarCollapsed:false 
    };
    
    $rootScope.checkLeftScrollbarVisible = function() {
        var containerElem = $('#sidebar-container');
        layoutStatus.leftScrollbarVisible = containerElem.get(0).scrollHeight > containerElem.height();
    };
}