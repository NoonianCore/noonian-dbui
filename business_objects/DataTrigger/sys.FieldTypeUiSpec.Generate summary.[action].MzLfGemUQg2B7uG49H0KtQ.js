function(_) {
    
    var summary='';
    
    const typeNames = _.get(this, 'fieldtypes.length') ? _.map(this.fieldtypes, 'name').join(', ') : '*';
    
    const e = this.for_editing, v = this.for_viewing, ea = this.for_editing_array, va = this.for_viewing_array;
    
    var uses = [];
    
    this.for_viewing && uses.push('View');
    this.for_editing && uses.push('Edit');
    this.for_viewing_array && uses.push('View list of');
    this.for_editing_array && uses.push('Edit list of');
    
    if(uses.length) {
        this.summary = uses.join(' or ')+' '+typeNames;
    }
    else {
        this.summary = 'disabled';
    }
    
}