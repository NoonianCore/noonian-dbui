function (scope, iElement, iAttributes, ngModel, $filter) {
    
    
    //When a change to ngModel occurs, $render is called to notify us to update the UI
    // we'll pull out viewValue into scope driving the UI
    ngModel.$render = function() {
        var vv = ngModel.$viewValue;
        scope.stringVal = vv.stringVal;
    };
    
    
    const getLastValid = function() {
        //We store the last valid JSON object in the viewValue as lastValidObj
        var vv = ngModel.$viewValue || {};
        return vv.objVal || vv.lastValidObj;
    };
    
    
    //When the string value changes on the scope,
    //  attempt parse and update viewValue
    scope.$watch('stringVal', function(stringVal) {
        // console.log('$watch(stringVal)', stringVal);
        var lastValidObj = getLastValid();
        if(!stringVal) {
            ngModel.$setViewValue({
                stringVal,
                lastValidObj
            });
            ngModel.$setValidity('validJson', false);
            return;
        }
        
        try {
            var objVal = angular.fromJson(stringVal);
            ngModel.$setViewValue({
                stringVal,
                objVal,
                lastValidObj:objVal
            });
            ngModel.$setValidity('validJson', true);
        } catch(e) {
            ngModel.$setViewValue({
                stringVal,
                lastValidObj
            });
            ngModel.$setValidity('validJson', false);
        }
    });
      
    
    //Formatter: convert actual ng-model object -> internal viewValue
    ngModel.$formatters.push(function(modelValue) {
        //object -> JSON string
        // console.log('format', modelValue);
        
        if(angular.isDefined(modelValue)) {
            return {
                objVal:modelValue,
                lastValidObj:modelValue,
                stringVal:$filter('json')(modelValue)
            };
        }
        else {
            return {stringVal:''};
        }
    });
    
    //Parser:  convert our internal viewValue -> value actually stored in ng-model object
    ngModel.$parsers.push(function(vv) {
        // console.log('parse', vv);
        return vv.objValue || vv.lastValidObj;
    });
        
        
    
    //TODO the following is copy/pasted from sourcecode field editr; refactor to move to library
    var wrapperElem = iElement.find('.ace-wrapper');
    
    var dragBarElem = iElement.find('.ace-dragbar');
    
    var dragging = false;
    
    var minHeight = 75;
    var maxHeight = 1000;
    
    //Prevent mousewheel from propagating to the main page when mouse is over
    // the ace editor that is fully scrolled to the top or bottom.
    // (annoyance when working in the editor)
    wrapperElem.bind('wheel', function(e) {
        e.preventDefault();
    });
    
    dragBarElem.on('mousedown', function(e) {
        e.preventDefault();
    	dragging = true;
    	
    	var editorElem = iElement.find('.ace_editor');
    	var contentElem = iElement.find('.ace_content');
    	
    	var top_offset = editorElem.offset().top;
    
    	// Set editor opacity to 0 to make transparent so our wrapper div shows
    	editorElem.css( 'opacity', 0.1 );
    	
    	
    
    	// handle mouse movement
    	$( document ).mousemove( function ( e ) {
    
    		var actualY = e.pageY;
    		// editor height
    		var eheight = Math.max(actualY - top_offset, minHeight);
    		
    		// Set wrapper height
    		
    		wrapperElem.css( 'height', eheight);
    		$('body').css('cursor','row-resize');
    		contentElem.css('cursor','row-resize');
    		
    		// Set dragbar opacity while dragging (set to 0 to not show)
    		dragBarElem.css( 'opacity', 0 );
    		
    	} );
    });
    
    $( document ).mouseup( function ( e ) {
    	if ( dragging ) 
    	{
    		var editorElem = iElement.find('.ace_editor');
    		var contentElem = iElement.find('.ace_content');
    
    		var actualY = e.pageY;
    		var top_offset = editorElem.offset().top;
    		var eheight = Math.max(actualY - top_offset, minHeight);
    
    		$( document ).unbind( 'mousemove' );
    		
    		// Set dragbar opacity back to 1
    		dragBarElem.css('opacity', '' );
    		
    		$('body').css('cursor','');
    		contentElem.css('cursor','');
    		
    		// Set height on actual editor element, and opacity back to 1
    		editorElem.css( 'height', eheight ).css( 'opacity', 1 );
    		
    		// Trigger ace editor resize()
    		scope.aceEditor.resize();
    		dragging = false;
    	}
    	
    } );
    
}