function ($scope, Dbui, $timeout) {
    
    Dbui.getRoleMap().then((roleMap)=> {
        
        $scope.allRoles = [];
        _.forEach(roleMap, function(name, id){
            $scope.allRoles.push({
                _id:id,
                name:name
            });
        });
        
        $timeout(()=>{
            $scope.$emit(Dbui.EVENTS.fieldInitComplete, this);
        });
        
    });

}