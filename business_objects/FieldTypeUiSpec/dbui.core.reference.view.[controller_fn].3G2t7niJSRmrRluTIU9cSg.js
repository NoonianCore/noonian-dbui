function ($scope, DbuiAction) {
    $scope.viewRefObj = function() {
      var td = $scope.typeDesc;
      var fc = $scope.fieldCustomizations;
      
      var displayValue = $scope.displayValue;
      if(displayValue) {
          var action = fc.viewAction || 'view';
          
          if(typeof action === 'string') {
              action =  DbuiAction.unalias(action);
          }
          
          var args = {
              className:  td.ref_class || displayValue.ref_class
          };
          DbuiAction.invokeAction(null, displayValue._id, action, args);
      }
    };

}