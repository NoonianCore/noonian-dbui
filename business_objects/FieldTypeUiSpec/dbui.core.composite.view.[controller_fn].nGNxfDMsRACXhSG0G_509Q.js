function ($scope, $stateParams, Dbui) {
    
    var displayValue = $scope.displayValue;
    var fc = $scope.fieldCustomizations || {};
    $scope.useViewer = false;
    //If composite field being viewed has a _disp, that's what is displayed.
    // otherwise a dbui-object-viewer directive is used.
    if(displayValue && (!displayValue._disp || fc.view_perspective)) {
        $scope.useViewer = true;
        //'composite' type descriptors have a 'construct' function which instantiates a proper BO-like 'sub-object'
        var td = $scope.typeDesc;
        var stub = td.construct({});
        
        if(fc.view_perspective) {
            var p = angular.copy(fc.view_perspective);
            if(p.layout) {
                p.layout = Dbui.normalizeLayout(p.layout);
            }
            $scope.subPerspective = p;
        }
        else {
            Dbui.getPerspective($stateParams.perspective, stub._bo_meta_data.class_name, 'view').then(function(subPerspective) {
                $scope.subPerspective = subPerspective;
            });
        }
    }
    
}