function ($scope, Dbui, $timeout) {

    $scope.itemTypeDesc = $scope.typeDesc[0];
    
    const triggerDirty = function() {
        $scope.binding.value = _.clone($scope.binding.value); //reassign array to trigger ngmodel dirty status
    };
    
    const absorbChildInitEvents = (evt, specOb) => {
        evt.stopPropagation();
    };

    $scope.editing = -1;

    $scope.addItem = function() {
      if(!$scope.binding.value) {
        $scope.binding.value = [];
      }
      
      var objArray = $scope.binding.value;

      objArray.push(null);
      $scope.editing = objArray.length-1;
      
      var deregFn = $scope.$on(Dbui.EVENTS.fieldInitComplete, absorbChildInitEvents);
      //After the scope settles, replace the bound array so ngModel dirties
        // and deregister the notification absorber
        $timeout(()=>{
            triggerDirty();
            deregFn();
        })
    };

    $scope.editItem = function(index) {
      $scope.editing = index;
    };

    $scope.removeItem = function(index) {
      $scope.binding.value.splice(index, 1);
      triggerDirty();
    };

    $scope.doneEditing = function() {
      $scope.editing = -1;
    };
    
    $scope.moveUp = function(index) {
        var arr = $scope.binding.value;
        if(index > 0) {
            var tmp = arr[index-1];
            arr[index-1] = arr[index];
            arr[index] = tmp;
            triggerDirty();
        }
    };
    
    $scope.moveDown = function(index) {
        var arr = $scope.binding.value;
        if(index < arr.length -1) {
            var tmp = arr[index+1];
            arr[index+1] = arr[index];
            arr[index] = tmp;
            triggerDirty();
        }
    };

  }