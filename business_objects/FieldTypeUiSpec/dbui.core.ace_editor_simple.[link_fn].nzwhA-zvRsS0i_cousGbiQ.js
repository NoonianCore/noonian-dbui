function (scope, iElement, iAttributes) {
    var wrapperElem = iElement.find('.ace-wrapper');
    
    var dragBarElem = iElement.find('.ace-dragbar');
    
    var dragging = false;
    
    var minHeight = 75;
    var maxHeight = 1000;
    
    //Prevent mousewheel from propagating to the main page when mouse is over
    // the ace editor that is fully scrolled to the top or bottom.
    // (annoyance when working in the editor)
    wrapperElem.bind('wheel', function(e) {
        e.preventDefault();
    });
    
    dragBarElem.on('mousedown', function(e) {
        e.preventDefault();
    	dragging = true;
    	
    	var editorElem = iElement.find('.ace_editor');
    	var contentElem = iElement.find('.ace_content');
    	
    	var top_offset = editorElem.offset().top;
    
    	// Set editor opacity to 0 to make transparent so our wrapper div shows
    	editorElem.css( 'opacity', 0.1 );
    	
    	
    
    	// handle mouse movement
    	$( document ).mousemove( function ( e ) {
    
    		var actualY = e.pageY;
    		// editor height
    		var eheight = Math.max(actualY - top_offset, minHeight);
    		
    		// Set wrapper height
    		
    		wrapperElem.css( 'height', eheight);
    		$('body').css('cursor','row-resize');
    		contentElem.css('cursor','row-resize');
    		
    		// Set dragbar opacity while dragging (set to 0 to not show)
    		dragBarElem.css( 'opacity', 0 );
    		
    	} );
    });
    
    $( document ).mouseup( function ( e ) {
    	if ( dragging ) 
    	{
    		var editorElem = iElement.find('.ace_editor');
    		var contentElem = iElement.find('.ace_content');
    
    		var actualY = e.pageY;
    		var top_offset = editorElem.offset().top;
    		var eheight = Math.max(actualY - top_offset, minHeight);
    
    		$( document ).unbind( 'mousemove' );
    		
    		// Set dragbar opacity back to 1
    		dragBarElem.css('opacity', '' );
    		
    		$('body').css('cursor','');
    		contentElem.css('cursor','');
    		
    		// Set height on actual editor element, and opacity back to 1
    		editorElem.css( 'height', eheight ).css( 'opacity', 1 );
    		
    		// Trigger ace editor resize()
    		scope.aceEditor.resize();
    		dragging = false;
    	}
    	
    } );
    
    return true;
}