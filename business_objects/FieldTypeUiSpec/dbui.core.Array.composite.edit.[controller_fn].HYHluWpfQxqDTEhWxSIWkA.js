function ($scope, $stateParams, Dbui, $timeout) {

    //Little editable datatable
    
    var fc = $scope.fieldCustomizations || {};
    var td = $scope.typeDesc[0];
    
    var stub = td.construct({});
    
    $scope.objMetaData = stub._bo_meta_data;
    
    
    const triggerDirty = function() {
        $scope.binding.value = _.clone($scope.binding.value);
    };
    
    //Action object to remove an item from the array:
    var removeAction = {
      icon:'fa-times',
      fn:function(args) {
        if(args) {
          $scope.binding.value.splice(args.index, 1);
          triggerDirty();
        }
        return true;
      }
    };
    
    const swapFn = function(offset, args) {
        let i = args && args.index;
        let arr = $scope.binding.value;
        let j = i+offset;
        if(j >=0 && j < arr.length) {
            let saved = arr[j];
            arr[j] = arr[i];
            arr[i] = saved;
        }
        return true;
    };
    
    const moveUpAction = {
        icon:'fa-arrow-up',
        fn:swapFn.bind(null, -1)
    };
    
    const moveDownAction = {
        icon:'fa-arrow-down',
        fn:swapFn.bind(null, 1)
    };
    
    
    const aliasActions = {
        remove:removeAction,
        move_up:moveUpAction,
        move_down:moveDownAction
    }
    
    //Config object for 
    $scope.tableConfig = {
      alwaysEdit:true,    //always show editor (don't switch between viewer and editor)
      cellEdit:true,      //all fields permitted to be edited
      noShrinkWrap:true,
      recordActions:[     //actions to be appended to any perspective actions.
          'remove','move_up','move_down'
      ],
      aliasActions
    };
    
    if(fc.table_config) {
        _.assign($scope.tableConfig, fc.table_config);
    }
    
    
    //Use 'list' perspective for editing an array of composites:
    if(fc.list_perspective) {
        var p = angular.copy(fc.list_perspective);
        p.layout = Dbui.normalizeLayout(p.layout);
        p.recordActions = p.recordActions || [];
        $scope.subPerspective = p;
        $scope.$emit(Dbui.EVENTS.fieldInitComplete, this);
    }
    else {
        Dbui.getPerspective($stateParams.perspective, stub._bo_meta_data.class_name, 'list').then((p)=>{
            p.recordActions = [];
            p.tableActions = [];
            $scope.subPerspective = p;
            $scope.$emit(Dbui.EVENTS.fieldInitComplete, this);
        });
    }
    
    
    //Ensure empty array binding when binding.value is null:
    var unwatchFn = $scope.$watch('binding.value', function(newBinding) {
        if(!newBinding) {
            $scope.binding.value = [];
        }
        unwatchFn();
    });
    
    // Absorb fieldInitComplete to avoid resetting dirty status when new rows are added
    // (table creates field editors that emit the event!) 
    const absorbChildInitEvents = (evt, specOb) => {
        // console.log('ABSORB', specObj.key, evt.targetScope===$scope);
        evt.stopPropagation();
    }
    
    
    
    
    
    $scope.addItem = function() {
        var deregFn = $scope.$on(Dbui.EVENTS.fieldInitComplete, absorbChildInitEvents);
        
        var newObj = td.construct({});
        $scope.binding.value.push(newObj);
        
        //After the scope settles, replace the bound array so ngModel dirties
        // and deregister the notification absorber
        $timeout(()=>{
            triggerDirty();
            deregFn();
        })
        
    }

}